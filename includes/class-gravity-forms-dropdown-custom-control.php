<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * User: ryansmith
 * Date: 10/11/18
 * Time: 7:43 AM
 *
 * Class to create custom customizer control for Gravity Forms
 */
if ( class_exists( 'WP_Customize_Control' ) ) {
	class Gravity_Forms_Dropdown_Custom_control extends WP_Customize_Control {
		/**
		 * Render the content on the theme customizer page
		 */
		public function render_content() {

			?>
            <label>
                <span class="customize-post-type-dropdown"><?php echo esc_html( $this->label ); ?></span>
                <select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
					<?php
					$forms = GFAPI::get_forms();
					foreach ( $forms as $form ) {
						echo '<option value="' . $form['id'] . '" ' . selected( $this->value, $form['id'] ) . '>' . $form['title'] . '</option>';
					}
					?>
                </select>
            </label>
			<?php
		}
	}
}