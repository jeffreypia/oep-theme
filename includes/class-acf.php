<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * ACF extensions, such as WYSIWYG toolbar adjustments
 *
 * @since   1.0.0
 * @package oep
 */
class Oep_ACF {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'acf/init'                    , [ __CLASS__ , 'set_google_api_key'   ] );
		add_filter( 'acf/fields/wysiwyg/toolbars' , [ __CLASS__ , 'set_wysiwyg_toolbars' ] );
		add_filter( 'acf/settings/show_admin'     , [ __CLASS__ , 'maybe_disable_admin'  ] );
		add_filter( 'acf/settings/save_json'      , [ __CLASS__ , 'set_json_save_path'   ] );
		add_filter( 'acf/settings/load_json'      , [ __CLASS__ , 'set_json_load_path'   ] );
	}


	/**
	 * Set Google API key
	 *
	 * @since 1.0.0
	 */
	public static function set_google_api_key() {
		acf_update_setting( 'google_api_key', OEP_GAPI );
	}


	/**
	 * Set WYSIWYG field toolbars
	 *
	 * @param  array  $toolbars  existing WYSIWYG toolbar fields
	 * @return array  $toolbars
	 *
	 * @since  1.0.0
	 */
	public static function set_wysiwyg_toolbars( $toolbars ) {

		// core "basic" options
		$basic = array(
			'bold', 'italic', 'underline', 'strikethrough',
			'undo', 'redo', 'link', 'unlink',
		);
		$toolbars['Very Basic'][1] = $basic;

		// additional "with headings and lists" options
		array_unshift( $basic, 'formatselect' );
		$basic[] = 'bullist';
		$basic[] = 'numlist';
		$toolbars['Very Basic with Headings and Lists'][1] = $basic;

		return $toolbars;
	}


	/**
	 * Disable ACF admin if production environment
	 *
	 * @return boolean
	 * @since  1.0.0
	 */
	public static function maybe_disable_admin() {
		return ! oep_is_prod();
	}


	/**
	 * Save field group to parent theme if it's already there
	 *
	 * @param  string  $path  where the JSON is going to save
	 * @return string  $path  the parent theme's /acf-json, potentially
	 *
	 * @since 1.2.0
	 */
	public static function set_json_save_path( $path ) {

		global $post;

		// bounce if no post (like if duplicating)
		if ( empty( $post ) ) {
			return $path;
		}

		// json file name is the field group's name
		$file_name = $post->post_name . '.json';

		// if the file already exists in the parent theme, re-save it there
		$parent_path = get_template_directory() . '/acf-json';

		if ( file_exists( "$parent_path/$file_name" ) ) {
			return $parent_path;
		}

		return $path;
	}


	/**
	 * Load field groups in parent so they can by syn'c while in child theme
	 *
	 * @param  array  $paths  places for ACF to look for JSON
	 * @return array  $paths
	 *
	 * @since 1.2.0
	 */
	public static function set_json_load_path( $paths ) {
		$paths[] = get_template_directory() . '/acf-json';
		return $paths;
	}
}

new Oep_ACF;
