<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Button features
 *
 * Builder, shortcode handler, etc.
 *
 * @since   1.0.0
 * @package oep
 */
class Oep_Button {

	/**
	 * Default button args
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $defaults = [
		'tag'      => 'a',
		'id'       => null,
		'type'     => null,
		'value'    => null,
		'link'     => null,
		'url'      => null,
		'text'     => '',
		'class'    => [],
		'icon'     => '',
		'icon_r'   => false,
		'new_tab'  => false,
		'tabindex' => null,
	];


	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action(    'oep_button' , [ __CLASS__ , 'do_button'    ], 10, 3 );
		add_shortcode( 'button'       , [ __CLASS__ , 'do_shortcode' ]        );
	}


	/**
	 * Get a button
	 *
	 * $button['link'] comes from an ACF "link" field. This builder allows you
	 * to use that OR have explicitly defined text, url, and/or new_tab.
	 *
	 * @param  array   $button   main button options (see parse_args below)
	 * @param  array   $query    builds query string
	 * @param  array   $data     builds data attrs
	 * @return bool    false     invalid options/settings
	 * @return string  $button
	 *
	 * @since  1.0.0
	 */
	public static function get_button( $button, $query = [], $data = [] ) {

		// parse all args
		$button = self::parse_args( $button );

		// create button vars
		extract( $button );

		// check text/url requirements
		if (
			( ! $url && ! $link['url'] && $tag == 'a' ) ||
			( ! $text && ! $link['title'] )
		) return false;

		// configure href, text, and icon
		$href = $url  ?: $link['url'];
		$text = $text ?: $link['title'];
		$icon_fam = apply_filters( 'oep_button_icon_family', 'fas' );
		$icon = $icon ?  "<span class='icon'><i class='$icon_fam fa-$icon'></i></span>" : '';

		// right icon?
		if ( $icon_r && $icon ) {
			$icon_r = $icon;
			$icon   = '';
		}

		// no href if not anchor
		$href = $tag != 'a' ? null : $href;

		// build attributes
		$attrs = oep_attrs([
			'href'     => $query ? add_query_arg( $query, $href ) : $href,
			'role'     => 'button',
			'id'       => $id,
			'type'     => $type,
			'value'    => $value,
			'target'   => $new_tab || $link['target'] == '_blank' ? '_blank' : '',
			'class'    => $class,
			'data'     => $data,
			'tabindex' => $tabindex,
		]);

		// put together
		return "<$tag $attrs>$icon<span class='text'>$text</span>$icon_r</$tag>";
	}


	/**
	 * Output a button
	 *
	 * @see   self::get_button()
	 * @since 1.0.0
	 */
	public static function do_button( $button, $query = [], $data = [] ) {
		echo self::get_button( $button, $query, $data );
	}


	/**
	 * Handle shortcode
	 *
	 * Set up new tab "toggle" so just just new-tab can be put in the shortcode
	 * instead of new-tab="true"
	 *
	 * @param  array   $atts     shortcode attributes
	 * @param  string  $content  button text
	 * @return string            button
	 */
	public static function do_shortcode( $atts, $content = null ) {

		$a = shortcode_atts([
			'url'  => '#',
			'icon' => '',
		], empty( $atts ) ? [] : $atts );

		$a['new-tab'] = array_search( 'new-tab', $atts ) !== false;

		return self::get_button([
			'text'    => esc_attr( $content ),
			'url'     => esc_url( $a['url'] ),
			'new_tab' => $a['new-tab'],
			'icon'    => esc_attr( $a['icon'] ),
		]);
	}


	/**
	 * Apply defaults to button args
	 *
	 * @param  array  $args  button args
	 * @return array  $args  button args parsed with defaults + ACF link defaults
	 *
	 * @since  1.0.0
	 */
	private static function parse_args( $args ) {

		$args = wp_parse_args( $args, self::$defaults );

		$args['link'] = wp_parse_args( $args['link'], [
			'title'  => '',
			'url'    => '',
			'target' => '',
		]);

		return $args;
	}
}

new Oep_Button;
