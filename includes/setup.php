<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since   1.0.0
 * @package oep
 */
function oep_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on oep, use a find and replace
	 * to change 'oep' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'oep', OEP_DIR . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in multiple locations.
	register_nav_menus([
		'off-canvas'  => esc_html__( 'Global Off-Canvas Sidebar', 'oep' ),
		'primary'     => esc_html__( 'Primary', 'oep' ),
		'footer'      => esc_html__( 'Footer', 'oep' ),
		'footer-meta' => esc_html__( 'Footer Meta', 'oep' ),
		'error'       => esc_html__( 'Error', 'oep' ),
	]);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', [
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	]);

	/*
	 * Add Root's Soil Support
	 *
	 * Notes: jQuery CDN moved to scripts class because SOIL no longer supports
	 * it. 'disable-custom-colors' is the Gutenberg custom color picker thing.
	 */
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-disable-trackbacks' );
	add_theme_support( 'soil-nav-walker' );
	add_theme_support( 'soil-nice-search' );
	add_theme_support( 'disable-custom-colors' );
}

// this file is being included at after_setup_theme now!
oep_setup(); // add_action( 'after_setup_theme', 'oep_setup' );



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global integer  $content_width
 */
function oep_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'oep_content_width', 1200 );
}
add_action( 'after_setup_theme', 'oep_content_width', 0 );


/**
 * global default banner when none provided
 *
 * @since 1.0.0
 */
function oep_set_global_banner() {
	define( 'OEP_GLOBAL_BANNER', get_theme_mod( 'banner_image' ) ?? null );
}
add_action( 'init', 'oep_set_global_banner' );
