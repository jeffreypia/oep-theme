<?php
/**
 * Misc. helper functions
 *
 * @since   1.0.0
 * @package oep
 */

/**
 * General check if OEP plugin is active
 *
 * @return bool
 * @since  1.0.0
 */
function oep_active() {
	return class_exists( 'OEP\Common\Plugin' );
}

/**
 * Are any of these things empty()?
 *
 * Checks all arguments to see if any of them are empty(). Alternative to:
 * if ( empty( this ) || empty( that ) ) {}.
 *
 * @param  mixed
 * @return bool
 *
 * @since  1.0.0
 */
function oep_any_empty() {

	return ( count(
		array_filter(
			func_get_args(),
			function( $v ){ return empty( $v ); } // wrapper needed for empty()
		)
	) > 0 );
}


/**
 * Break array into string of attributes
 *
 * Has special cases for "class" and "data" keys if their values are arrays, so
 * it's compatible with arrays of data attributes (by using recusion).
 *
 * @param  array  $attrs   array of data attributes (keys) and their values
 * @param  string $prefix  a prefix for the data attribute (ex: "data-")
 * @return string          inline string of data attributes
 *
 * @since  1.0.0
 */
function oep_attrs( $attrs, $prefix = '' ) {

	foreach ( $attrs as $attr => &$value ) {

		// remove if empty
		if ( empty( $value ) ) {
			unset( $attrs[ $attr ] );
			continue;
		}

		// if data- attributes
		if ( $attr == 'data' && is_array( $value ) ) {
			$attrs[ $attr ] = oep_attrs( array_filter( $value ), 'data-' );
			continue;
		}

		// if array of classes
		if ( $attr == 'class' && is_array( $value ) ) {
			$value = implode( ' ', array_filter( $value ) );
		}

		// array of classes + all other cases
		$attrs[ $attr ] = $prefix . $attr . '="' . esc_attr( $value ) . '"';
	}

	return apply_filters( 'oep_attrs', implode( ' ', $attrs ), $attrs );
}


/**
 * Wrapper for oep_attrs, but just for class
 *
 * @param  array   $classes  classes to send to oep_attrs()
 * @return string
 *
 * @since  1.2.0
 */
function oep_attrs_class( $classes ) {
	return oep_attrs( [ 'class' => $classes ] );
}


/**
 * Get/build a button
 *
 * @see   Oep_Button::get_button
 * @since 1.0.0
 */
function oep_get_button( $button, $query = [], $data = [] ) {
	return Oep_Button::get_button( $button, $query, $data );
}


/**
 * Output a button
 *
 * @since 1.0.0
 */
function oep_button( $button, $query = [], $data = [] ) {
	echo oep_get_button( $button, $query, $data );
}


/**
 * Get a piece of contact information
 *
 * @see    class-contacts/get_contact
 * @since  1.0.0
 */
function oep_get_contact( $field, $group = 'orl' ) {
	return Oep_Contacts::get_contact( $field, $group );
}


/**
 * Output a piece of contact information
 *
 * @see   oep_get_contact
 * @since 1.0.0
 */
function oep_contact( $field, $group = 'orl' ) {
	echo oep_get_contact( $field, $group );
}


/**
 * Get an address
 *
 * @see  class-contacts/get_address
 * @since 1.0.0
 */
function oep_get_address( $group = 'orl', $html = true, $link = true ) {
	return Oep_Contacts::get_address( $group, $html, $link );
}


/**
 * Output an address
 *
 * @see   oep_get_address
 * @since 1.0.0
 */
function oep_address( $group = 'orl', $html = true, $link = true ) {
	echo oep_get_address( $group, $html, $link );
}


/**
 * Output WYSIWYG content into a div
 *
 * @param string  $content
 * @since 1.0.0
 */
function oep_entry_content( $content ) {
	echo $content ? '<div class="entry-content">' . $content . '</div>' : '';
}


/**
 * Get an SVG file so it can be used inline
 *
 * @param  string  $name   the file name of the svg in the theme's images folder, minus .php
 * @param  class   $class  an extra class to add to the wrapper
 * @return string          contents of SVG file
 *
 * @since  1.0.0
 */
function oep_get_svg( $name, $class = null ) {

	if ( empty( $name ) || ! is_string( $name ) ) {
		return;
	}

	// asset location
	$asset = "/assets/images/$name.svg";

	// check child (active) theme first
	if ( file_exists( OEP_CHILD_DIR . $asset ) ) {
		$svg = OEP_CHILD_DIR . $asset;
	}

	// then check parent
	elseif ( file_exists( OEP_DIR . $asset ) ) {
		$svg = OEP_DIR . $asset;
	}

	// sanity check: nothing found
	else {
		return;
	}

	// config file location
	// $svg = OEP_DIR . '/assets/images/' . $name . '.svg';

	// // make sure something is there
	// if ( ! file_exists( $svg ) ) {
	// 	return;
	// }

	$class = oep_attrs([ 'class' => [
		'svg', $name, $class,
	]]);

	// get the file
	ob_start();
	include $svg;
	return "<div $class>" . ob_get_clean() . "</div>";
}


/**
 * Output an inlined SVG
 *
 * @see   oep_get_svg() for params
 * @since 1.0.0
 *
 * @TODO update this function to manage multiple SVGs based on site parameters
 */
function oep_svg( $name, $class = null ) {
	echo oep_get_svg( $name, $class );
}


/**
 * Get current page template filename without the ".php"
 *
 * @return string         current page template filename without .php
 * @return bool    false  template not found
 *
 * @since  1.0.0
 */
function oep_get_page_template() {

	$template = basename( get_page_template() );

	if ( $template ) {
		return str_replace( '.php', '', $template );
	} else {
		return false;
	}
}


/**
 * Get a video src from an iframe
 *
 * @param  string  $iframe  iframe code returned by ACF
 * @return string           video URL / src attribute with autoplay
 *
 * @see    https://developers.google.com/youtube/player_parameters
 * @since  1.0.0
 */
function oep_get_iframe_src( $iframe, $autoplay = true, $controls = true ) {

	// match the src attribute
	preg_match( '/src="(.+?)"/', $iframe, $matches );

	// sanity check
	if ( empty( $matches[1] ) ) return '';

	return add_query_arg([
		'autoplay'       => (int) $autoplay,
		'controls'       => (int) $controls,
		'modestbranding' => 1,
		'rel'            => 0,
		'fs'             => 0,
		'color'          => 'white',
	], $matches[1] );
}


/**
 * Sanitize a string or single-dimensional array
 *
 * @param  mixed  $value  string or array to sanitize
 * @return mixed          array or string with 99.9% less germs
 *
 * @since  1.0.0
 */
function oep_array_sanitize( $value ) {

	if ( gettype( $value ) == 'string' ) {
		return sanitize_text_field( $value );
	}

	// call again if array
	elseif ( is_array( $value ) ) {
		return array_map( __FUNCTION__, $value );
	}

	else {
		throw new Error( 'oep_array_sanitize is for arrays and strings only' );
	}
}


/**
 * Get a meta/customizer-friendly key name
 *
 * @param  string  $name
 * @return string
 *
 * @since 1.0.0
 */
function oep_format_key_name( $name ) {
	return str_replace( '-', '_', strtolower( sanitize_file_name( $name ) ) );
}


/**
 * Lower Yoast SEO meta box priority
 *
 * @return string  meta box priority
 * @since  1.0.0
 */
function oep_set_yoast_priority() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'oep_set_yoast_priority' );


/**
 * Load a section template part with an ACF Field Group
 *
 * Just pass the slug and name of the template part you need, specifying a clone
 * field's name as necessary.
 *
 * Meta is filtered with these hooks, with ACF field name and additional args
 * passed to the callbacks:
 * @see meta/{slug}
 * @see meta/{slug}/{$name}
 *
 *
 * @param string             $slug        slug name for the generic template
 * @param string|null        $name        name of specialized template
 * @param string|array|null  $meta_field  An array of meta, or the (clone) field name. if
 *                                        meta is passed via $args, that takes priority.
 * @param array|null         $args        Additional data to extract()
 *
 * @since 1.0.0
 */
function oep_section( $slug, $name = null, $meta_field = null, $args = [] ) {

	do_action( "oep_section_{$slug}", $slug, $name );

	// auto-assume options page ID equaling a CPT key for an archive!
	$post_id = is_archive() ? get_post_type() : get_the_ID();
	$post_id = apply_filters( 'oep_section_post_id', $post_id, $slug, $name );

	// no meta passed (through args)? check field name.
	if ( empty( $args['meta'] ) ) {

		// if $meta_field is array, use that
		if ( is_array( $meta_field ) ) {
			$args['meta'] = $meta_field;
		}

		// if a string (for a field name), check for the field. Fall back to an
		// array if not found.
		elseif ( is_string( $meta_field ) ) {
			$args['meta'] = $meta_field ? ( get_field( $meta_field, $post_id ) ?: [] ) : [];
		}

		// if there still isn't anything, try to match a field with the slug + _name
		elseif ( ! $meta_field ) {
			$field_name   = oep_format_key_name( $slug . ( $name ? "_$name" : '' ) );
			$args['meta'] = get_field( $field_name, $post_id ) ?: [];
		}

		// if all else fails, default empty
		else {
			$args['meta'] = [];
		}
	}


	// apply meta filters
	$args['meta'] = apply_filters( "meta/{$slug}"        , $args['meta'], $meta_field, $args );
	$args['meta'] = apply_filters( "meta/{$slug}/{$name}", $args['meta'], $meta_field, $args );


	// load template part
	oep_template_part( $slug, $name, $args );
}


/**
 * Get a template part from the theme
 *
 * @param string  $slug  the slug name for the generic template
 * @param string  $name  the name of the specialized template
 * @param array   $args  arguments passed to oep_load_template
 *
 * @since 1.0.0
 */
function oep_template_part( $slug, $name = null, $args = [] ) {

	// allow edge cases to get in here
	do_action( "get_template_part_{$slug}", $slug, $name );

	// build the path
	$path = OEP_DIR . '/template-parts/' . $slug;

	// add specialized name
	$path .= $name ? "-$name" : '';

	$path .= '.php';

	// check for the file
	if ( file_exists( $path ) ) {
		oep_load_template( $path, false, $args );
	}
}


/**
 * Load template
 *
 * Modified version of WordPress' load_template function with an added argument
 * to pass variables to the required template part to avoid needing
 * memory-hogging global vars.
 *
 * This implementation is similar to what WooCommerce does.
 *
 * @param string  $_template_file  path to the template file
 * @param bool    $require_once    whether to require_once or require
 * @param array   $args            variables extracted to template part
 *
 * @since 1.0.0
 */
function oep_load_template( $_template_file, $require_once = true, $args = [] ) {

	global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

	if ( is_array( $wp_query->query_vars ) ) {
		extract( $wp_query->query_vars, EXTR_SKIP );
	}

	// modification
	if ( is_array( $args ) ) {
		extract( $args, EXTR_SKIP );
	}

	if ( isset( $s ) ) {
		$s = esc_attr( $s );
	}

	if ( $require_once ) {
		require_once( $_template_file );
	} else {
		require( $_template_file );
	}
}


/**
 * Output numbered pagination
 *
 * @since 1.0.0
 */
function oep_numbered_pagination($echo = true, $base = '') {

	global $wp_query;

	$big   = 999999999; // need an unlikely integer
	$links = paginate_links([
		'base'               => $base ?: str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'             => '?paged=%#%',
		'current'            => max( 1, $echo ? get_query_var( 'paged' ) : intval(get_query_var( 'paged' ) + 1) ),
		'total'              => $wp_query->max_num_pages,
		'mid_size'           => 2,
		'end_size'           => 2,
		'prev_text'          => __( 'Previous', 'oep' ),
		'next_text'          => __( 'Next', 'oep' ),
		'type'               => 'list',
		'before_page_number' => '<span class="screen-reader-text">' . __( 'Page', 'oep' ) . ' </span>',
	]);

	if($echo) {
		echo '<nav class="posts-navigation numbered-pagination">' . $links . '</nav>';
	} else {
		return '<nav class="posts-navigation numbered-pagination">' . $links . '</nav>';
	}
}

// let plugins hook in safely
add_action( 'oep_numbered_pagination', 'oep_numbered_pagination' );

/**
 * Ajax paginaiton
 *
 * @since 1.0.0
 */

function oep_ajax_pagination(){
	$query_vars = apply_filters('ajax_query_vars', json_decode( stripslashes( $_POST['query_vars'] ), true )); //allow plugins to override values

	$query_vars['paged'] = $_POST['page'] ?: get_query_var('paged');
	$query_vars['post_type'] = isset($_POST['post_type']) ? $_POST['post_type'] : get_query_var('post_type');
	$base = isset($_POST['base_url']) ? $_POST['base_url'] : '';

	error_log(json_encode($query_vars));

	$posts = new WP_Query( $query_vars );
	$GLOBALS['wp_query'] = $posts;

	add_filter( 'editor_max_image_size', 'oep_image_size_override' );

	if( ! $posts->have_posts() ) {
		echo "<article><p>".__("We couldn't find what you were looking for. Please try again.", "oep")."</p></article>";
	}
	else {
		while ( $posts->have_posts() ) {
			$posts->the_post();
			$post_type = get_post_type();

			if(is_author()){
				get_template_part( 'template-parts/content', 'author' );
			}

			else{
				$part = apply_filters( "{$post_type}_ajax_pagination_part", 'content-grid' );
				get_template_part( "template-parts/$part", $post_type );
			}
		}
	}
	remove_filter( 'editor_max_image_size', 'oep_image_size_override' );

	oep_numbered_pagination(true, $base );

	die();
}

add_action( 'wp_ajax_nopriv_ajax_pagination', 'oep_ajax_pagination' );
add_action( 'wp_ajax_ajax_pagination', 'oep_ajax_pagination' );

/**
 * Editor image size override for AJAX calls
 *
 * @since 1.0.0
 */
function oep_image_size_override() {
	return array( 720, 440 );
}

/**
 * Add breakpoint testing
 * Don't edit this block. If you need breakpoint test enabled, simply open your wp-config-local.php file and add
 *
 * @example define("BREAKPOINT_TEST", true);
 *
 * @since 1.0.0
 */
function oep_add_breakpoint_test() { ?>
	<div class="breakpoint-test"><span class="name"></span><span class="size"></span></div>
<?php }
(!defined("BREAKPOINT_TEST") || (defined( BREAKPOINT_TEST) && BREAKPOINT_TEST === false)) ?: add_action( 'wp_footer', 'oep_add_breakpoint_test', 100 );


/**
 * Let the admin know if SOIL isn't active
 *
 * @since 1.0.0
 */
function oep_add_soil_notice() {

	if ( class_exists( '\\Roots\soil\\Options' ) ) {
		return;
	}

	$message = sprintf(
		__( 'This theme requires the %sSoil%s plugin to work properly!' ),
		'<b><a href="https://github.com/roots/soil/releases" target="_BLANK">', '</a></b>'
	);

	echo '<div class="notice notice-error"><p>' . $message . '</p></div>';

}
add_action( 'admin_notices', 'oep_add_soil_notice' );


/**
 * Is the site currently in production?
 *
 * Checks for wp-config-local 'DZ_IS_PROD' definition or live Pantheon
 * environment super
 *
 * @return string   'pantheon'  Pantheon "live" environment detected
 * @return string   'local'     'DZ_IS_PROD' defined in wp-config-local.php
 * @return boolean  false       neither production variable found
 *
 * @since  1.0.0
 */
function oep_is_prod() {

	if ( defined( 'DZ_IS_PROD' ) && DZ_IS_PROD ) {
		return 'local';
	} elseif ( isset( $_ENV['PANTHEON_ENVIRONMENT'] ) && $_ENV['PANTHEON_ENVIRONMENT'] === 'live' ) {
		return 'pantheon';
	} else {
		return false;
	}
}


/**
 * Get the label for the production environment
 *
 * @return string   how it's determined as a "production" environment
 * @return boolean  false  not a production environment
 *
 * @since 1.0.0
 */
function oep_get_prod_label() {

	switch ( $prod = oep_is_prod() ) {

		case 'local' :
			return __( 'Local config', 'oep' );
			break;

		case 'pantheon' :
			return __( 'Live Pantheon Environment', 'oep' );
			break;

		default :
			return false;
			break;
	}
}

/**
 * Render share icons
 *
 * @since 0.2.0
 */
function render_social_sharing_icons() {
	if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) {
		$share_buttons = ADDTOANY_SHARE_SAVE_KIT( [ 'output_later' => true, 'kit_additional_classes' => 'a2a_vertical_style', 'icon_bg' => 'transparent', 'icon_fg' => 'custom', 'icon_bg_color' => 'transparent', 'icon_fg_color' => 'transparent' ]);
		$share_buttons = oep_strip_styles( $share_buttons );
		echo $share_buttons;

		/**
		 * @TODO the icons actually get injected by an externally hosted js file, so no filtering actually available.
		 */

	}
}

/**
 * Strip style attributes
 *
 * @since 0.2.0
 */

function oep_strip_styles( $html ) {
	//return preg_replace('/(<[^>]*) style=("[^"]+"|\'[^\']+\')([^>]*>)/i', '$1$3', $html);
	return preg_replace('/ style=("|\')(.*?)("|\')/','',$html);

}



/**
 * Link DZ and notify of production environment in admin footer
 *
 * @param  string  existing footer text
 *
 * @return string
 * @since  1.0.0
 */
function oep_set_admin_footer( $text ) {

	$site_by = sprintf(
		__( 'Site by %sDesignzillas%s.', 'oep' ),
		'<a href="https://www.designzillas.com/" target="_BLANK">', '</a>'
	);

	$prod = oep_get_prod_label();
	$prod = $prod ? sprintf(
		__( '%sProduction%s', 'oep' ),
		'<b><abbr title="' . esc_attr( $prod ) . '">', '</abbr></b>'
	) . ' ' : '' ;

	return ( $text ? "$text | " : '' ) . $prod . $site_by;
}
add_filter( 'admin_footer_text', 'oep_set_admin_footer' );

/**
 * stop autop
 */

//remove_filter ('the_content', 'wpautop');

add_filter( 'the_content', 'remove_autop_for_posttype', 0 );

function remove_autop_for_posttype( $content )
{
	# edit the post type here
	'post' !== get_post_type() && remove_filter( 'the_content', 'wpautop' );
	return $content;
}


/**
 * Remove categories from admin
 */

function oep_remove_tax() {
	register_taxonomy('category', array());
}

function oep_remove_sub_menus() {
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
}
add_action('admin_menu', 'oep_remove_sub_menus');
add_action('init', 'oep_remove_tax');

/**
 * remove media category dropdowns from posts listing
 */

function oep_remove_media_taxonomy_dropdown($taxonomies){
	if(is_customize_preview()){
		return;
	}
	$screen = get_current_screen();
	$post_types =array_merge( [ "post" ], \OEP\CPT\PostType::get_cpt_keys());
	if(in_array($screen->post_type, $post_types) && $screen->action !== "add") {
	    return null; //bug out, don't need the dropdowns on the post listing page
    }
	return $taxonomies;
}
add_filter("media-taxonomies", "oep_remove_media_taxonomy_dropdown");

/**
 * dequeue media taxonomies script on post listings
 */

function oep_remove_media_taxonomy_script() {
    //need this in customizer
    if(is_customize_preview()){
        return;
    }
	$screen = get_current_screen();
	$post_types =array_merge( [ "post" ], \OEP\CPT\PostType::get_cpt_keys());
	if(in_array($screen->post_type, $post_types) && $screen->action !== "add") {
		wp_dequeue_script( "media-taxonomies" );
	}
}

add_action( 'admin_enqueue_scripts', 'oep_remove_media_taxonomy_script', 99);


/**
 * Kint debugger (var_dump fallback) wrapper that's easier to find
 *
 * @param mixed  whatever you want
 * @since 1.0.0
 */
function ALAN() {
	call_user_func_array( function_exists( 'd' ) ? 'd' : 'var_dump', func_get_args() );
}

/**
 * fix acf breaking media upload for options pages
 * @see https://github.com/themosis/framework/issues/27
 */

add_action('admin_enqueue_scripts', function()
{
	wp_enqueue_media();
});


/**
 * Output content grid thumbnail
 *
 * @since 1.1.0
 */
function oep_do_content_grid_thumbnail() {
	oep_cover_image( oep_get_bg_image( $bg_image ?? null ) ?: OEP_GLOBAL_BANNER, 'large' );
}
add_action( 'oep_content_grid_thumbnail', 'oep_do_content_grid_thumbnail' );


/**
 * Show event date in "content grid" for event CPT
 *
 * @since 1.1.0
 */
function oep_do_event_date() {

	$date = new DateTime( get_field( 'start_date' ) );
	?>

	<div class="event_date">
		<strong><?php echo date_format( $date, 'd' ); ?></strong>
		<?php echo date_format( $date, 'M' ); ?>
	</div>
	<?php
}
add_action( 'oep_content_grid_oep_cpts_event_thumbnail', 'oep_do_event_date' );


/**
 * Output an archive's "override page" blocks
 *
 * Just a wrapper.
 *
 * @since 1.2.0
 */
function oep_get_archive_blocks() {
	return Oep_Archive_Blocks::get_blocks();
}


/**
 * Open success stories wrapper
 *
 * @param string  $type  post type
 * @since 1.2.0
 */
function oep_do_success_stories_wrap_open( $type ) {
	echo $type == 'oep_cpts_success' ? '<section class="success-stories"><div class="wrap">' : '';
}
add_action( 'oep_before_archive_loop_posts', 'oep_do_success_stories_wrap_open' );


/**
 * Close success stories wrapper
 *
 * @param string  $type  post type
 * @since 1.2.0
 */
function oep_do_success_stories_wrap_close( $type ) {
	echo $type == 'oep_cpts_success' ? '</div></section>' : '';
}
add_action( 'oep_after_archive_loop_posts', 'oep_do_success_stories_wrap_close', 5 );


/**
 * Open neighborhoods wrapper
 *
 * @param string  $type  post type
 * @since 1.2.0
 */
function oep_do_neighborhoods_wrap_open( $type ) {
	echo $type == 'oep_cpts_hood' ? '<div class="wrap">' : '';
}
add_action( 'oep_before_archive_loop_posts', 'oep_do_neighborhoods_wrap_open' );


/**
 * Close neighborhoods wrapper
 *
 * @param string  $type  post type
 * @since 1.2.0
 */
function oep_do_neighborhoods_wrap_close( $type ) {
	echo $type == 'oep_cpts_hood' ? '</div>' : '';
}
add_action( 'oep_after_archive_loop_posts', 'oep_do_neighborhoods_wrap_close', 5 );


/**
 * Open the company cards container
 *
 * @param string  $type  post type
 * @since 1.2.0
 */
function oep_do_company_cards_wrap_open( $type ) {
    echo $type == 'oep_cpts_company' ? '<section class="company-cards"><div class="company-cards_container results">' : '';
}
add_action( 'oep_before_archive_loop_posts', 'oep_do_company_cards_wrap_open' );


/**
 * Close the company cards container
 *
 * @param string  $type  post type
 * @since 1.2.0
 */
function oep_do_company_cards_wrap_close( $type ) {
    echo $type == 'oep_cpts_company' ? '</div></section>' : '';
}
add_action( 'oep_after_archive_loop_posts', 'oep_do_company_cards_wrap_close', 5 );
