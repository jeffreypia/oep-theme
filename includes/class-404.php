<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * 404 Setup
 *
 * Register 404 options page, set post_id for _section, etc.
 *
 * @since   1.0.0
 * @package oep
 */
class Oep_404 {

	/**
	 * Post ID for options page / fields
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $post_id = 'page_404';


	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'acf/init'              , [ __CLASS__ , 'add_options_page' ] );
		add_filter( 'oep_section_post_id' , [ __CLASS__ , 'set_post_id'      ] );
	}


	/**
	 * Add the 404 options page
	 *
	 * @since 1.0.0
	 */
	public static function add_options_page() {

		acf_add_options_sub_page([
			'post_id'     => self::$post_id,
			'page_title'  => __( '404 Page', 'oep' ),
			'menu_title'  => __( '404', 'oep' ),
			'parent_slug' => 'edit.php?post_type=page',
			'menu_slug'   => 'page-404',
		]);
	}


	/**
	 * Set the post ID for 404 page sections
	 *
	 * @param  mixed  $post_id  existing post_id to switch to 404 (if needed)
	 * @return mixed            existing post_id or 404 page options post_id
	 *
	 * @since  1.0.0
	 */
	public static function set_post_id( $post_id ) {
		return is_404() ? self::$post_id : $post_id;
	}
}

new Oep_404;
