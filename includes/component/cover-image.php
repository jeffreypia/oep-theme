<?php
/**
 * Poly-filled "cover" image with source sets
 * used as an alternative to inline background
 * images using CSS
 *
 * @param   integer  $attachment  media attachment ID
 * @param   string   $size        image size to get
 * @return  string                image element with polyfill data attribute
 *
 * @since   1.0.0
 * @package oep
 */
function oep_get_cover_image( $attachment, $size = 'thumbnail' ) {
	return wp_get_attachment_image( $attachment, $size, false, [
		'data-object-fit' => 'cover',
	]);
}


/**
 * Output a cover image
 *
 * @see   get_cover_image for params
 * @since 1.0.0
 */
function oep_cover_image( $attachment, $size = 'thumbnail' ) {
	echo oep_get_cover_image( $attachment, $size );
}
