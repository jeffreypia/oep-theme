<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package oep
 */
if ( ! function_exists( 'oep_get_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function oep_get_posted_on() {

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated sr-only xxx" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date('M j.Y') )
		);

		$posted_on = sprintf(
			esc_html_x( '%s', 'post date', 'oep' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
		return '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.
	}
endif;

if ( ! function_exists( 'oep_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function oep_posted_on() {
		echo oep_get_posted_on();
	}
endif;

if ( ! function_exists( 'oep_get_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 *
	 * @param $region where do you want to output this?
	 */
	function oep_get_posted_by( $region = "banner" ) {
		if(is_404()){
			return;
		}
		OEP\Common\oep_get_author_block( $region );
	}
endif;

if ( ! function_exists( 'oep_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 *
	 * @param $region where do you want to output this?
	 */
	function oep_posted_by( $region = "banner" ) {
		echo oep_get_posted_by( $region );
	}
endif;

if ( ! function_exists( 'oep_get_bg_image' ) ) :
	/**
	 * Get the featured image of a post whenever a banner image isn't available
	 *
	 * @param $bg_image attachment id of banner image
	 *
	 * @return string
	 */
	function oep_get_bg_image( $bg_image ) {
		if(is_404()){
			return false;
		}
		return ! empty( $bg_image ) ? $bg_image : get_post_thumbnail_id( get_the_ID() );

	}
endif;

if ( ! function_exists( 'oep_get_pills' ) ) :
	/**
	 * Get the term pills
	 * Should be a wrapper for a OEP\Taxonomies\Taxonomy::get_pills()
	 * don't break the site if the class doesn't exist
	 *
	 * @param $type string
	 * @param $taxonomies array
	 *
	 * @return string
	 *
	 */
	function oep_get_pills($type = 'solid', $taxonomies = []) {
		return class_exists( 'OEP\Taxonomies\Taxonomy' ) ? OEP\Taxonomies\Taxonomy::get_pills($type, $taxonomies) : null;
	}
endif;

if ( ! function_exists( 'oep_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function oep_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'oep' ) );
			if ( $categories_list && oep_categorized_blog() ) {
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'oep' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma
			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'oep' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'oep' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}*/
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			/* translators: %s: post title */
			comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'oep' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
			echo '</span>';
		}
	}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function oep_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'oep_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'oep_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so oep_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so oep_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in oep_categorized_blog.
 */
function oep_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'oep_categories' );
}

add_action( 'edit_category', 'oep_category_transient_flusher' );
add_action( 'save_post', 'oep_category_transient_flusher' );
