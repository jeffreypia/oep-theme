<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Handler for getting blocks on an archive page
 *
 * This pulls a specific page set in an option to pull blocks from to display in
 * an archive template instead of the default "The Loop". "The Loop" will be in
 * a "Archive Posts" block instead.
 *
 * This feature requires theme support, which can be added to a theme in
 * 'after_setup_theme' with add_theme_support( 'oep-archive-block-page' )
 *
 * @since   1.2.0
 * @package oep
 */
class Oep_Archive_Blocks {

	/**
	 * Supported post types
	 *
	 * Make sure the "Page Content" field group is added to the options page
	 * "location" so the post page selector field is available.
	 *
	 * @var   array
	 * @since 1.2.0
	 */
	private static $post_types = [
		'oep_cpts_success',
		'oep_cpts_hood',
		'oep_cpts_company',
	];


	/**
	 * Override page post object field name
	 *
	 * @var   string
	 * @since 1.2.0
	 */
	private static $post_object_field = 'block_page';


	/**
	 * Get things going
	 *
	 * @since 1.2.0
	 */
	function __construct() {
		add_action( 'acf/init'                     , [ __CLASS__, 'admin_init'         ]      );
		add_action( 'admin_bar_menu'               , [ __CLASS__, 'add_bar_edit_link'  ] , 50 );
		add_action( 'oep_archive_loop'             , [ __CLASS__, 'do_loop_wrap_open'  ] , 10 );
		add_action( 'oep_archive_loop'             , [ __CLASS__, 'do_loop'            ] , 30 );
		add_action( 'oep_archive_loop'             , [ __CLASS__, 'do_loop_wrap_close' ] , 50 );
		add_action( 'oep_after_archive_loop_posts' , 'oep_numbered_pagination'                );
	}


	/**
	 * Hook in functionality if theme has support
	 *
	 * @since 1.2.0
	 */
	public static function admin_init() {

		if ( ! is_admin() || ! self::has_support() ) {
			return;
		}

		$name = self::$post_object_field;

		add_action( "wp_loaded"                     , [ __CLASS__, 'add_option_pages'    ]        );
		add_filter( "acf/update_value/name={$name}" , [ __CLASS__, 'set_post_visibility' ], 10, 3 );
	}


	/**
	 * Does the theme have support for this?
	 *
	 * @return boolean
	 * @since  1.2.0
	 */
	private static function has_support() {
		return current_theme_supports( 'oep-archive-block-page' );
	}


	/**
	 * Define an options subpage for each supported post type
	 *
	 * @since 1.2.0
	 */
	public static function add_option_pages() {
		array_walk( self::$post_types, [ __CLASS__, 'add_options_page' ] );
	}


	/**
	 * Add an options subpage for a CPT
	 *
	 * @since 1.2.0
	 */
	private static function add_options_page( $post_type ) {

		if ( ! $post_type = get_post_type_object( $post_type ) ) {
			return;
		}

		acf_add_options_sub_page([
			'page_title'  => sprintf( __( '%s Page Settings' ), $post_type->labels->menu_name ),
			'menu_title'  => $post_type->labels->menu_name, //sprintf( __( '%s Page' ), $post_type->labels->menu_name ),
			'menu_slug'   => "{$post_type->name}-page-settings",
			'parent_slug' => add_query_arg( 'post_type', 'page' /*$post_type->name*/, 'edit.php' ),
			'post_id'     => $post_type->name,
		]);
	}


	/**
	 * Set whatever post is set as the override to "Privately Published"
	 *
	 * We aren't actually filtering/changing the field value.
	 *
	 * @param  string   $value    field value
	 * @param  integer  $post_id  options page ID
	 * @param  array    $field    field information
	 * @return string   $value    the unchanged value
	 *
	 * @since 1.2.0
	 */
	public static function set_post_visibility( $value, $post_id, $field ) {

		if ( $value && $field['type'] == 'post_object' ) {

			wp_update_post([
				'ID'          => $value,
				'post_status' => 'private',
			]);
		}

		return $value;
	}


	/**
	 * Get the override page for the current archive/post type
	 *
	 * @return integer         post ID for override page
	 * @return boolean  false  not set or not found
	 *
	 * @since  1.2.0
	 */
	private static function get_override_page() {

		return function_exists( 'get_field' )
			? get_field( self::$post_object_field, get_post_type() )
			: false;
	}


	/**
	 * Get the blocks from the "override page" for an archive
	 *
	 * @return string
	 * @since  1.2.0
	 */
	public static function get_blocks() {

		return ( $page = self::get_override_page() )
			? do_blocks( get_the_content( null, false, $page ) )
			: false;
	}


	/**
	 * Add an edit link to the admin bar on overriden archive pages
	 *
	 * @param WP_Admin_Bar  $bar  admin bar object
	 * @since 1.2.0
	 */
	public static function add_bar_edit_link( $bar ) {

		// we should be on an archive page for a supported post type
		if ( is_admin() || ! is_archive() || ! in_array( get_post_type(), self::$post_types ) ) {
			return;
		}

		$page = self::get_override_page();
		$href = $page
			? get_edit_post_link( $page )
			: add_query_arg([
				'post_type' => 'page',
				'page'      => get_post_type() . '-page-settings',
			], admin_url( 'edit.php' ) );

		$bar->add_menu([
			'id'    => 'oep-archive-override-page-edit',
			'title' => '<span class="ab-icon dashicons dashicons-edit"></span>' . __( 'Edit Archive', 'oep' ),
			'href'  => $href,
			'meta'  => [
				'target' => '_self',
				'title'  => __( 'Edit Overriding Content', 'oep' ),
			],
		]);
	}


	/**
	 * Open the basic archive loop wrapper
	 *
	 * @since 1.2.0
	 */
	public static function do_loop_wrap_open() {

		$attrs = oep_attrs_class([
			'block-archive-loop',
			'archive-loop-' . get_post_type(),
			'full-width',
		]);
		echo "<div $attrs>";
	}


	/**
	 * Close the basic archive loop wrapper
	 *
	 * @since 1.2.0
	 */
	public static function do_loop_wrap_close() {
		echo '</div><!-- .block-archive-loop -->';
	}


	/**
	 * Do the the basic archive loop
	 *
	 * @since 1.2.0
	 */
	public static function do_loop() {

		$type = get_post_type();

		if ( have_posts() ) :

			/**
			 * Hooked
			 *
			 * @todo add a default "loop posts wrapper"?
			 */
			do_action( 'oep_before_archive_loop_posts', $type );

			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', $type );
			endwhile;

			/**
			 * Hooked
			 *
			 * Search theme(s) for extra post-type specific externsions that put
			 * wrappers and stuff.
			 *
			 * @todo add a default "loop posts wrapper"?
			 *
			 * oep_numbered_pagination - 10
			 */
			do_action( 'oep_after_archive_loop_posts', $type );

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
	}
}

new Oep_Archive_Blocks;
