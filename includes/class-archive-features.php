<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * "Featured" archive posts handling
 *
 * @since   1.2.0
 * @package archive
 */
class OEP_Archive_Features {

	/**
	 * Post types to run this stuff on
	 *
	 * @var   string
	 * @since 1.2.0
	 */
	private static $post_types = [];


	/**
	 * Featured post IDs
	 *
	 * @var   array
	 * @since 1.2.0
	 */
	private static $featured;


	/**
	 * Get things going (by default)
	 *
	 * @since 1.2.0
	 */
	function __construct() {

		if ( class_exists( 'OEP\Common\Plugin' ) ) {
			self::$post_types[] = OEP_SUCCESS_KEY;
		}

		add_action( 'acf/save_post'                 , [ __CLASS__ , 'set_featured' ]     );
		add_action( 'pre_get_posts'                 , [ __CLASS__ , 'set_query'    ]     );
		add_action( 'oep_featured'                  , [ __CLASS__ , 'do_featured'  ]     );
		add_action( 'oep_before_archive_loop_posts' , [ __CLASS__ , 'do_featured'  ], 20 );
	}


	/**
	 * Cache featured posts on post type save
	 *
	 * @param integer  $post_id  just saved post ID
	 * @since 1.2.0
	 */
	public static function set_featured( $post_id ) {

		if ( in_array( $post_id, self::$post_types ) ) {
			set_transient( "oep_featured_{$post_id}s", self::get_featured_query( $post_id ) );
		}
		elseif ( $post_id == Oep_Blog::get_page_ID() ) {
			set_transient( "oep_featured_posts", self::get_featured_query( $post_id, 'post' ) );
		}
	}


	/**
	 * Get a query of featured posts
	 *
	 * @param  mixed     $post_id  post ID for post or options page (ACF)
	 * @return boolean   false     no featured posts
	 * @return WP_Query
	 *
	 * @since 1.2.0
	 */
	private static function get_featured_query( $post_id, $post_type = null ) {

		if ( ! $featured = get_field( 'featured', $post_id ) ) {
			return false;
		}

		$posts = new WP_Query([
			'post__in' => $featured,
			'fields'   => 'ids',
			'post_type' => $post_type ?: $post_id,
		]);

		return ! $posts->have_posts() ?: $posts;
	}


	/**
	 * Get/cache featured posts
	 *
	 * @param  string  $type  post type
	 * @return array          empty or featured post IDs
	 *
	 * @since  1.2.0
	 */
	private static function get_featured( $type ) {

		$type = $type ?: 'post';

		if ( isset( self::$featured[ $type ] ) ) {
			return self::$featured[ $type ];
		}

		return self::$featured[ $type ] = get_transient( "oep_featured_{$type}s" );
	}


	/**
	 * Remove featured posts so they can be put in their own template part
	 *
	 * @param WP_Query  $query  query object
	 * @since 1.2.0
	 */
	public static function set_query( $query ) {

		if (
			   is_admin()
			|| ! $query->is_main_query()
			|| ( ! $query->is_home() && ! $query->is_post_type_archive( self::$post_types ) )
			|| ! $featured = self::get_featured( $query->get( 'post_type' ) )
		) return;

		$query->set( 'post__not_in', $featured->get_posts() );
	}


	/**
	 * Output featured posts
	 *
	 * @since 1.2.0
	 */
	public static function do_featured() {

		$type = get_post_type();

		if (
			   ! in_array( $type, self::$post_types )    // not supported post type
			|| get_query_var( 'paged' )                  // after first page
			|| ! $featured = self::get_featured( $type ) // no featured
		) return;

		oep_template_part( 'featured', '', [ 'featured' => $featured ]);

		
	}
}

new OEP_Archive_Features;
