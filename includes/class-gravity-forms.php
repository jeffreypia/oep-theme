<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Gravity Forms
 *
 * Misc Gravity Forms modifications/helpers
 *
 * @since   1.0.0
 * @package oep
 */
class Oep_Gravity_Forms {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'gform_init_scripts_footer', '__return_true' );
		add_filter( 'gform_submit_button', [ __CLASS__, 'set_submit_button' ], 10, 2 );
		add_filter( 'gform_ajax_spinner_url', [ __CLASS__, 'set_spinner_url' ] );
		add_filter( 'gform_field_css_class', [ __CLASS__, 'add_field_type_class' ], 10, 2 );
		add_filter( 'gform_field_content', [ __CLASS__, 'add_autocomplete_off' ], 10, 2 );
	}


	/**
	 * Apply the theme's button component to the Gravity Form submit
	 *
	 * This allows us to have a non-"input" DOM element that can be customized
	 * at the full capacity of our button function.
	 *
	 * @param  string $button existing Gravity Forms button HTML
	 * @param  array  $form   Gravity Form information
	 *
	 * @return string           our own button
	 *
	 * @since 1.0.0
	 */
	public static function set_submit_button( $button, $form ) {

		// get form ID & text
		$id           = $form['id'];
		$form_id      = absint( $form['id'] );
		$button_input = GFFormDisplay::get_form_button( $form['id'], "gform_submit_button_{$form['id']}", $form['button'], __( 'Submit', 'gravityforms' ), 'gform_button', __( 'Submit', 'gravityforms' ), 0 );
		//$button_input = gf_apply_filters( array( 'gform_submit_button', $form_id ), $button_input, $form );


		$dom = new DOMDocument;
		$dom->loadHTML( $button_input );
		$attr = array();
		foreach ( $dom->getElementsByTagName( 'input' ) as $tag ) {
			foreach ( $tag->attributes as $attribName => $attribNodeVal ) {
				$attr[ $attribName ] = $tag->getAttribute( $attribName );
			}
		}
		$text = ! empty( $form['button']['text'] ) ? $form['button']['text'] : __( 'Submit', 'gi-tax' );

		$args = array(
			'tag'      => 'button',
			'text'     => $text,
			'icon'     => '',
			'tabindex' => GFCommon::$tab_index ++,
		);

		$args = array_merge( $args, $attr );

		return oep_get_button( $args );
	}


	/**
	 * Set the AJAX spinner URL
	 *
	 * Get a spinner here (or something): https://loading.io
	 *
	 * @since 1.0.0
	 */
	public static function set_spinner_url() {
		return OEP_DIR_URI . '/assets/images/ajax.svg';
	}


	/**
	 * Add Gravity Form field type to field wrapper class(es)
	 *
	 * @param  string $classes existing classes, separated by space
	 * @param  object $field   field object
	 *
	 * @return string            new class(es)
	 *
	 * @since 1.0.0
	 */
	public static function add_field_type_class( $classes, $field ) {
		return $classes .= ' gfield_' . $field->get_input_type();
	}


	/**
	 * Turn "autocomplete" off on certain field types
	 *
	 * @param  string $field_content field HTML
	 * @param  object $field         field object
	 *
	 * @return string                  new HTML
	 *
	 * @since  1.0.0
	 */
	public static function add_autocomplete_off( $field_content, $field ) {

		if ( empty( $field->dateType ) || $field->dateType !== 'datepicker' ) {
			return $field_content;
		}

		return str_replace( "type=", "autocomplete='off' type=", $field_content );
	}
}

new Oep_Gravity_Forms;
