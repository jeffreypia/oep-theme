<?php
/**
 * Custom image sizes
 *
 * @link    https://developer.wordpress.org/reference/functions/add_image_size/
 * @since   1.0.0
 * @package oep
 */
function oep_add_image_sizes() {
	add_image_size( 'banner' , 1650 , 1200 );
    add_image_size( 'thumbnail' , 720 , 440, true );
}
add_action( 'init', 'oep_add_image_sizes', 20 );

function oep_add_image_sizes_names($sizes) {
	return array_merge( $sizes, array(
		'banner' => __('Banner'),
	) );
}
add_filter( 'image_size_names_choose', 'oep_add_image_sizes_names');
