<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Script/style handling
 *
 * @since   1.0.0
 * @package oep
 */
class Oep_Scripts {

	/**
	 * Asset location
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $assets = OEP_DIR_URI . '/assets/';


	/**
	 * Hook things
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'wp_enqueue_scripts', [ __CLASS__, 'add_scripts' ], 99 );
		add_action( 'script_loader_tag', [ __CLASS__, 'maybe_defer_script' ], 10, 2 );
		add_action( 'wp_footer', [ __CLASS__, 'add_webfonts' ] );
	}


	/**
	 * Enqueue scripts and styles
	 *
	 * @since 1.0.0
	 */
	public static function add_scripts() {

		$template = oep_get_page_template();

		// main CSS
		wp_enqueue_style( 'oep-theme', self::$assets . 'css/style.min.css', [], OEP_THEME_VERSION, 'all' );

		// styleguide dev
		if ( $template == 'styleguide' ) {
			wp_enqueue_style( 'oep-styleguide', self::$assets . 'css/styleguide.min.css', [ 'oep' ], OEP_THEME_VERSION, 'all' );
		}

		/**
		 * scroll spy
		 *
		 * @since 0.2.0
		 */
		wp_enqueue_script( 'scroll-spy', self::$assets . 'js/vendor/jquery-scrollspy.min.js', [ 'jquery' ], false, true );

        /**
         * featherlight
         *
         * @since 0.2.0
         */
        wp_enqueue_script( 'featherlight-js', '//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js', [ 'jquery' ], '1.7.13', true );
        wp_enqueue_style( 'featherlight-css', '//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css', [], '1.7.13', 'all' );

		/**
		 * flickity
		 *
		 * @since 0.2.0
		 */
		wp_enqueue_script( 'flickity', '//unpkg.com/flickity@2/dist/flickity.pkgd.min.js', [ 'jquery' ], false, true );
		wp_enqueue_script( 'flickity-fade', '//unpkg.com/flickity-fade@1/flickity-fade.js', [ 'jquery' ], '1.0.0', true );

		/**
		 * Quick Links prefetcher
		 *
		 * @since 0.2.0
		 */
//		wp_enqueue_script( 'quicklinks', self::$assets . 'js/vendor/quicklink.umd.js', null, false, true );

		//main
		wp_enqueue_script( 'oep-foot', self::$assets . 'js/foot.min.js', [ 'jquery' ], OEP_THEME_VERSION, true );

		// head scripts - only use if absolutely needed!
		// wp_enqueue_script( 'oep-head', $assets . 'js/head.min.js', [], OEP_THEME_VERSION, false );

		/**
		 * Font Awesome Pro icons
		 *
		 * @see https://fontawesome.com/get-started/svg-with-js
		 */
		wp_enqueue_script( 'font-awesome', '//pro.fontawesome.com/releases/v5.8.1/js/all.js' );

		// comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		/**
         * Localize footer script for ajax
         *
         * @since 0.2.0
         */
		global $wp_query;
		$big   = 999999999; // need an unlikely integer
		wp_localize_script(
			'oep-foot',
			'ajaxpagination',
            array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'query_vars' => json_encode( $wp_query->query ),
                    'spinner_url' => OEP_DIR_URI .'/assets/images/ajax.svg',
                    'base_url' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) )
            )
		);
	}


	/**
	 * Filter HTML script tag to MAYBE add the "defer" attribute
	 *
	 * @param  string $tag    existing <script> tag
	 * @param  string $handle registered handle
	 *
	 * @return string  $tag     new <script> with defer
	 *
	 * @since  1.0.0
	 */
	public static function maybe_defer_script( $tag, $handle ) {

		//$allow = [ 'font-awesome' ];
		$allow = [];

		if ( in_array( $handle, $allow ) ) {
			$tag = str_replace( ' src', ' defer src', $tag );
		}

		return $tag;
	}


	/**
	 * Load Google Fonts
	 *
	 * @since 1.0.0
	 */
	public static function add_webfonts() { ?>

        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {
              families: [
                'Merriweather:400i',
                'Andada'
              ],
              testStrings: {
                'Andada': '\u201c'
              }
            },
            custom: {
              families: [
                'Modelica Regular', 'Modelica Medium', 'Modelica Medium Italic', 'Modelica Bold', 'Modelica Extra Bold'
              ]
            }
          })
        </script>

	<?php }
}

new Oep_Scripts;
