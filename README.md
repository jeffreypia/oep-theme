Getting Started
===============

1. *Delete the .git folder* (leave the .gitignore)
2. Make sure the theme's folder is named after what it's for


3. **Case-sensitive** search for: `'oep'` and replace with: `'megatherium-is-awesome'` (text domain)
4. **Case-sensitive** search for: `oep_` and replace with: `megatherium_is_awesome_` (function names)
5. **Case-sensitive** search for: `Text Domain: oep` and replace with: `Text Domain: megatherium-is-awesome` in `style.css`
6. **Case-sensitive** search for: <code>&nbsp;oep</code> and replace with: <code>&nbsp;Megatherium_is_Awesome</code> THERE IS A SPACE IN FRONT! (docblocks)
7. **Case-sensitive** search for: `oep-` and replace with: `megatherium-is-awesome-` (prefixed handles)
8. **Case-sensitive** search for: `Oep` and replace with: `Megatherium_Is_Awesome` (PHP class names)
9. **Case-sensitive** search for: `OEP` and replace with: `MEGATHERIUM_IS_AWESOME` (defines/constants)
10. **Case-sensitive** search for: `'orl'` and replace with wherever the client's primary location is (customizer contact info)

11. Update the stylesheet header in `style.css`. The text domain is supposed to be the same as the theme's folder name, and should not contain underscores.
12. Add a theme screenshot
13. Install ACF, then go to *Custom Fields* and sync available fields



JavaScript Assets
=================

## Foot Includes

- Mobile navigation slide-down navigation & sub-menu togglers
- _S navigation script for accessibility
- Object-fit polyfill for IE and stuff

## Head Includes

- Nothing included here, but you can put anything that has to be enqueued in the `<head>` here #HopefullyNot

## Building

- NEVER edit `foot.js`, `foot.min.js`, `head.js`, or `head.min.js`.
- Stop BrowserSync
- Create a new JS file in the `foot` directory
- Add in the stuff you need, doing template check as necessary to make sure it doesn't run when it shouldn't; also keeping in mind that WordPress runs in no-conflict mode (see `navigation-animate.js` for jQuery wrapper)
- Start BrowserSync and change something in the new file - it should be minified & concatenated into `foot.min.js` (unless using head)




SCSS Assets
===========

Directories
-----------

- /components: re-usable element features/groups (ex: button)
  - button
  - form
  - responsive embed (usage: `.embed-container`)
  - WP-specific stuff
- /layout    : global or large layout elements (ex: header)
  - banner
  - header
  - navigation
  - footer
- /pages     : pages
- /vendor    : 3rd-party stuff (ex: Featherlight)


Main Partials
-------------

**`breakpoints`:** Where the mobile-first breakpoints map is defined. Make sure you use `menu-break` for where the navigation breaks from mobile to desktop, if it does. This allows us to easily adjust the break for new links without breaking other site elements

**`main`:** Main container/global body styles

**`normailze`:** browser-resetting norms

**`typography`:** Headings, paragraphs, anchors, lists, quotes, etc. Break down if needed

**`variables`**


Mixins
------

**`rem-calc()`:** Converts pixels to REMs! Very useful for font-size.

**`cover-image()`:** object-fit covers something with 100% width and height.

**`absolute-center()`:** Absolutely centers an element within its parent.

**`icon()`:** Sets to font-icon font family with normal weight & provided content.

**`bulleted-list()`:** Basic formatted bulleted list using a FontAwesome icon instead of a `disc` or something.

**`no-bottom-if-last()`:** Removes bottom margin if `last-child`.

**`no-top-if-first()`:** Removes top margin if `first-child`

**`fp()`:** Fluid Property #magic. Give a property, minimum px, maximum px, and optionally the range where you'd like those to start and end. For example: `fp( 'font-size', 20, 40, 400, 1200 );` used on an `<h2>` would make its font size scale up from 20px (starting at 400px screen width) to 40px; stopping at 1200px screen width.

**`overlay()`:** Absolutely positions an element covering a container with 100% width and height with the specified z-index. Useful for background gradient/color overlays.

**`transition-ease-in-out`:** Generic ease-in-out transition, defaulting at 200ms.


Building
--------

- NEVER edit anything in the `assets/css` folder.
- Re-start BrowserSync after creating a new partial so gulp can watch it
- Normal SCSS workflow from there


Using the object-fit polyfill
-----------------------------

Instead of using dynamically-set css background images (such as a card grid with statically sized image headers using `background-size: cover;`), use `oep_cover_image()` (see below) to output an actual `<img>` with a sourceset, positioned with `object-fit: cover;`.



Theme functions (`functions.php`)
=================================

In an effort to keep theme-related functionality clean and modular, `functions.php` mostly just:

- sets our *THEME-RELATED* `define()`s
- requires our includes (see below)
- Is a decent place to add our Customizer sections (see class)
- Is a decent place to add our social profiles (see class)
- Sets our user_agent so `file_get_contents` and other things don't act up as much
- Moves Gravity Form scripts to the footer to help prevent jQuery issues



Includes
========

### `address` (component)
Outputs an `<address>` with the address customizer fields that come in `./functions.php`.


### `button` (component)
Use this to add buttons on the site. It basically takes in the result of `get_field()` with an ACF clone field.

**Functions**

- `oep_button()`: Takes in an array with settings for text, link, icon, new-tab, and/or an array of classes, as well as an optional array of query string arguments and builds a button out of it. This makes the button an easily callable global component that's easy to configure.
- `oep_get_button()`: A WP-style wrapper for getting the *string* of a button
- `oep_button_shortcode()`: Registers the `[button]` shortcode and passes it to the button builder for output. Ex. usage: `[button url="google.com" icon="google" new-tab]Go To Google[/button]`.



### `cover-image` (component)
This takes in an attachment ID (would be $image['ID'] from an ACF image field array) and a SIZE, then uses `wp_get_attachment_image()` to get the image including the `data-object-fit` attribute.


### `404`
Registers an ACF options page under *Pages* admin menu that you can add field groups to. Banner should already be added.


### `blog`
Blog-listing adjustments; adjusted "read more" excerpt included.


### `class-customizer`
Easily registers customizer settings.

Usage (see example in `functions.php`): use the class wrapper function and call `add_section()`: `Oep_Customizer::add_section( ...args...);`. The first parameter is the label for the Customizer section, the second an associative array with keys used for `get_theme_mod()` pointing to an array of customizer control options. However, if a default text field with no other options is desired, the value can just be a string.


### `class-socials`
Registers customizer settings and icons for social profile sharing.

Usage (see example in `functions.php`): Call `add_profile()` with profile name and its icon as the parameters. A URL field will appear in the Socials Customizer section for it. Use the `get_profiles()` method to get an array of all the socials and the `get_icon_nav()` method to return a `<nav>` containing icon links to the social profiles.


### `enqueue-scripts`
Enqueue CSS and JS


### `extras`
Random author tags and stuff that came with _S


### `functions`
Helper functions (see below)


### `get-field` (DEPRECATED -- See `oep_section()` and `page.php` + `banner.php` for example! )
Provides a clean way to get a clone field's fields inside a template part. See `template-parts/banner.php` for an example.

**Basic Usage:**

- Register an ACF field group
- Add the fields that would be rendered in the individual template part. Be as modular as reasonably possible!
- In the field group for the end-page (or end-post; ex: the "Home Page" field group ), add a *clone* field to "All *whatever the field group you just made is* fields" with *Display* set to "Grouped" and *Prefix Field Names* ON.
- Use `extract( oep_get_field( 'CLONE_FIELD_NAME' ) );` at the beginning of the template part, which localizes all the fields in the original field group that was cloned: So a field with the name "*title*" in the cloned group is now `$title` in the template part!

**Dynamic Archive/Options page usage:**

Let's say we want the banner of a "Portfolio" CPT archive template to be editable. By default, you can't click the "Edit Page" link in the admin toolbar like in single posts/pages and get the field group, so we need to register an options page and have the archive template grab the options:

1. Use `acf_add_options_sub_page()` to create an options page with the *ID equalling the custom post type's slug with an `options_` prefix*. This is most cleanly done in the CPT's PHP class as a method hooked into the constructor, this is more basic example:

	```
	function oep_portfolio_options() {

		$cpt_slug = 'portfolio';
		$label    = 'Portfolio Page';

		acf_add_options_sub_page( array(
			'post_id'		=> 'options_' . $cpt_slug,
			'page_title'	=> $label . ' Settings',
			'menu_title'	=> $label,
			'parent_slug'	=> 'edit.php?post_type=' . $cpt_slug,
			'menu_slug'		=> $cpt_slug,
		));
	}
	add_action( 'after_setup_theme', 'oep_portfolio_options' );
	```

2. Edit the conditional fields on the field group (such as banner) to be included on the new options page.
3. When `extract( oep_get_field( 'banner' ) );` is called in the Banner's template part, it will automatically recognize that the page is an archive and try to find options that match that archive's CPT!
4. $$$

**`oep_get_fields()`**

There's also a wrapper function for `oep_get_field()` that takes in multiple field names and loops over them.


### `gravity-forms`
Helpers for Gravity Forms

1. Move form init scripts to footer
2. Change the default AJAX spinner to one that isn't lame
3. Add the field type to each field wrappers' classes!


### `image-sizes`
Custom image size definitions. `banner` is included.


### `setup`
Mostly theme support declarations. [SOIL]( https://github.com/roots/soil/releases ) plugin support is declared here!



Helper Functions (`includes/functions.php`)
======================================

### `oep_any_empty()`
Checks if any of the arguments passed into the function are empty.
Ex: instead of `if ( empty( $this ) || empty( $that ) || empty( $yolo ) ) {}`, do `oep_any_empty( $this, $that, $yolo )`.

### `oep_attrs()`
Breaks an array of attributes (keys) and their values into a string that can be put into an element. Special cases for *class* and *data* keys if their values are arrays:

*class:* If an array of class names, it's `implode()`d into `class="array-item-1 array-item-2"`.
*data:* If an array of `data` keys and values, they're each added as `data-key="value"`.

*EXAMPLE*

```
$attrs = oep_attrs( array(
	'href'		=> 'http://example.com',
	'target'	=> '_BLANK",
	'class'		=> array( 'gold', 'chicken' ),
	'data'		=> array( 'design' => 'zilla' ),
));
echo "<a $attrs">Linky Link</a>";
```

results in:
`<a href="http://example.com" target="_BLANK" class="gold chicken" data-design="zilla">Linky Link</a>`


### `oep_get-svg()`
Grab the contents of an svg by file name (without the _.svg_ extension) from an asset directory set in the function. Makes it easy to get inlined SVGs for styling and better cross-browser compatibility with much cleaner code.


### `rock_get_page_template()`
Gets the current page's template (without the _.php_). Useful for conditional statments in global template parts.


### `oep_get_iframe_src()`
Get a video SRC from an iframe, optionally adding the `&autoplay` parameter. *Useful for ACF oEmbed fields!*


### `oep_array_sanitize()`
Sanitize a string or single-dimensional array using `sanitize_text_field()`.


-----


Gulp Tasks & Building
---------------------

* Update Node
* Update NPM
* cd to theme root in terminal
* run `npm install`
* Set BrowserSync's init proxy url to your dev url in `gulpfile.js`
* Before building, run `gulp browsersync` and go to the url it gives you
* If adding new SCSS partials or js files, you'll need to quit out of BrowserSync and restart once they're in the directory
