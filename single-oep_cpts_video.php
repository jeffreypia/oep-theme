<?php
/**
 * The template for displaying all video posts.
 *
 * @since   1.0.0
 * @package oep
 */

get_header(); ?>

	<main class="wrap" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		<aside>
			<?php
				do_action('related_post');
				do_action('newsletter');
			?>
		</aside>

	</main>
    <section class="additional-items wrap">
		<?php do_action('recommended_stories'); ?>
		<?php do_action('trending_stories'); ?>
    </section>


<?php get_footer();
