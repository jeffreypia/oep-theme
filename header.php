<?php
/**
 * Header template
 *
 * @since   1.0.0
 * @package oep
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f47b20">
	<meta name="msapplication-TileColor" content="#7ebd43">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'oep' ); ?></a>

	<header role="banner">

		<?php oep_template_part( 'header/pre-menu' ); ?>

		<div class="site-menu">
			<?php oep_template_part( 'header/logo', 'toggle' ); ?>
			<?php oep_template_part( 'header/navigation' ); ?>

			<div class="search-bar" id="search">
				<?php get_search_form(); ?>
			</div>
		</div>

	</header>

	<?php oep_template_part( 'header/sidebar', 'menu' ); ?>

	<div id="content">
