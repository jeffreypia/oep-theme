<?php
/**
 * The template for displaying archive pages.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main wrap" role="main">

			<?php
			if ( have_posts() ) : global $count; ?>

            <header class="page-header">
                <div class="accent" role="presentation" style="color: <?php \OEP\Taxonomies\Taxonomy::get_color(); ?>"
                     data-color="<?php \OEP\Taxonomies\Taxonomy::get_color(); ?>">
					<?php oep_svg( 'heading' ); ?>
                </div>
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description"><p class="detail">', '</p></div>' );
				?>
            </header>

            <div class="results">
                <h2><?php _e('Latest', 'oep'); ?></h2>
                <!-- types -->
                <div class="field types">
                    <label for="post_type>"><?php _e( 'Filter by', 'oep' ); ?></label>
		            <?php \OEP\Common\oep_post_type_select(); ?>
                </div>
                <?php do_action('taxonomy_filter_form'); //@TODO to be filled in later ?>
				<?php
				/* Start the Loop */
				$count = 0;
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content-grid', get_post_type() );

				endwhile;

				oep_numbered_pagination();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer();
