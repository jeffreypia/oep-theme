<?php
/**
 * Footer template
 *
 * Contains the closing of the #content div and all content after.
 *
 * @since   1.0.0
 * @package oep
 */
?>

</div><!-- #content -->

<footer role="contentinfo">

    <?php do_action( 'oep_pre_footer', 'orange', 'left' ); ?>

    <div class="wrap">

        <!-- menu -->
        <section class="wrap footer-menu">
            <?php wp_nav_menu([
                'theme_location' => 'footer',
                'depth'          => 2,
                'fallback_cb'    => false,
            ]); ?>
        </section>


        <section class="wrap address">
            <!-- address -->
			<?php oep_svg( 'logo-white' ) ?>
			<?php if ( $phone = oep_get_contact( 'phone' ) ) : ?>
                <a class="phone-link" href="tel:<?php echo $phone ?>"><i
                            class="fas fa-phone"></i><?php oep_contact( 'phone_display' ) ?></a>
			<?php endif; ?>
			<?php oep_address( 'orl' ); ?>
			<?php Oep_Socials::icon_nav(); ?>

        </section>

		<!-- newsletter //-->
		<div class="newsletter">
			<p><?php _e('Sign up for email updates'); ?></p>

			<div class="hubspot-form" data-hs-portal-id="270308" data-hs-form-id="648a2239-89b3-4c1f-8dc1-527065a1efe5"></div>
		</div>

        <!-- copyright -->
        <section class="copyright">
            <div class="wrap">
            	<p>

                    &copy; <?php echo date( 'Y' ) . ' ' . __( 'Orlando Economic Partnership', 'oep' ); ?>. <br/>
					<?php _e( 'All Rights Reserved', 'oep' );  ?>. 
					<?php _e( 'Website by', 'oep' ); ?>

					<?php if ( is_front_page() ) : ?>
                        <a href="https://www.designzillas.com" rel="nofollow"><?php _e( 'Designzillas', 'oep' ); ?></a>
					<?php else :
						_e( 'Designzillas', 'oep' );
					endif; ?>

                </p>
            </div>

            <div class="wrap footer-sub-menu">
                <?php wp_nav_menu([
                    'theme_location' => 'footer-meta',
                    'depth'          => 1,
                    'fallback_cb'    => false,
                ]); ?>
            </div>
        </section>
    </div><!-- .wrap -->

    <?php do_action('oep_after_footer'); ?>
</footer>

<?php wp_footer(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140171966-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-140171966-1');
</script>

</body>
</html>
