<?php
/**
 * Front Page Template
 *
 * @since   1.0.0
 * @package oep
 */

get_header(); ?>

    <?php do_action("oep_header"); ?>
	<main role="main">
		<?php
		get_template_part( 'template-parts/content', get_post_type() );
		?>
	</main>

    <section class="additional-items wrap">
        <?php do_action('trending_stories'); ?>
    </section>

<?php get_footer();
