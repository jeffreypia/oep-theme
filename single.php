<?php
/**
 * The template for displaying all single posts.
 *
 * @since   1.0.0
 * @package oep
 */

get_header(); ?>

	<?php oep_section( 'banner' ); ?>

	<main class="wrap" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.

		if( get_post_type() != OEP_EVENT_KEY ) :
		?>
		<aside>
			<?php
			do_action('upcoming_events');
			do_action('related_post');
			do_action('newsletter');
			do_action('featured_post');
			wp_reset_postdata();
			?>
		</aside>
		<?php endif; ?>
	</main>
<section class="additional-items wrap">
	<?php
	if( get_post_type() != OEP_EVENT_KEY ) :
		do_action('recommended_stories');
		do_action('trending_stories');
	elseif( get_post_type() == OEP_EVENT_KEY ) :
		do_action('recent_events');
	else :
		do_action('other_upcoming_events');
	endif;
	?>
</section>

<?php get_footer();
