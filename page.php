<?php
/**
 * Default Page
 *
 * @since   1.0.0
 * @package oep
 */

get_header(); ?>

	<?php oep_section( 'banner' ); ?>

	<main class="default" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php get_footer();
