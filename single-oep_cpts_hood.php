<?php
/**
 * Neighborhoods Single, grabbing landing page template
 *
 * @since   1.2.0
 * @package oep
 */

get_header();
?>

	<main role="main">
		<?php do_action("oep_header"); ?>

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'oep_cpts_lan-page' );

		endwhile; // End of the loop.
		?>

	</main>

<?php get_footer();
