<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
$filterby = urldecode(get_query_var("filterby"));
?>

	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'No events found. Please try again.', 'oep' ); ?></h1>
        <form method="GET" action="" id="eventFilter" class="event-filter">

            <div class="wrap">
                <label for="event-filter_select">
                    <span class="">Event filter:</span>
                    <select id="event-filter_select" name="filterby">
                        <option <?php echo !$filterby ? 'selected' : ''; ?> value="0">All upcoming events</option>
                        <option <?php echo 'today' === $filterby ? 'selected' : ''; ?> value="today">Today</option>
                        <option <?php echo ' 1 week' === $filterby ? 'selected' : ''; ?> value="+1 week">This week</option>
                        <option <?php echo 'last day of this month' === $filterby ? 'selected' : ''; ?> value="last day of this month">This month</option>
                        <option <?php echo ' 1 year' === $filterby ? 'selected' : ''; ?> value="+1 year">This year</option>
                    </select>
                </label>
            </div><!-- .wrap -->
        </form>
	</header><!-- .page-header -->
