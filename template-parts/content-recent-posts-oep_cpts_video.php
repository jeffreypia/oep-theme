<?php
/**
 * Template part for recent vidoe.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
?>


<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >

    <figure>
        <div class="post_thumbnail-wrap video">
			<?php
			do_action( 'media_thumbnail', $post);
			?>
        </div>
    </figure>
    <div>
		<?php
		global $count;
		oep_get_pills(
			'solid'
		);
		?>

    </div>
    <header class="entry-header">
        <h4 class="entry-title"><?php echo $post->post_title; ?></h4>
    </header>
    <footer class="entry-footer">
		<?php oep_posted_by( 'tile' ); ?> <?php oep_posted_on(); ?>
    </footer>
    <a class="post-link" href="<?php echo get_the_permalink(); ?>"></a>
</article>


