<?php
/**
 * Styleguide's colors section
 *
 * $args stuff is passed in from main styleguide template
 *
 * @since   1.0.0
 * @package oep
 */
?>

<h3><?php _ex( 'Colors', 'styleguide' ); ?></h3>

<dl class="colors">

	<dt class="layout"><?php _ex( 'Light Layout Colors', 'styleguide' ); ?></dt>
	<dd class="layout"><ul>
		<li class="secondary">HEX</li>
		<li class="gray">HEX</li>
		<li class="black">HEX</li>
		<li class="primary">HEX</li>
	</ul></dd>

	<dt class="layout"><?php _ex( 'Dark Layout Colors', 'styleguide' ); ?></dt>
	<dd class="layout"><ul>
		<li class="secondary">HEX</li>
		<li class="primary">HEX</li>
		<li class="white">HEX</li>
		<li class="gray">HEX</li>
	</ul></dd>

	<dt><?php _ex( 'Primary Interaction', 'styleguide' ); ?></dt>
	<dd class="interaction"><ul>
		<li class="primary-lighter">HEX</li>
		<li class="primary-light">HEX</li>
		<li class="primary">HEX</li>
		<li class="primary-dark">HEX</li>
		<li class="primary-darker">HEX</li>
	</ul></dd>

	<dt><?php _ex( 'Secondary Interaction', 'styleguide' ); ?></dt>
	<dd class="interaction"><ul>
		<li class="secondary-lighter">HEX</li>
		<li class="secondary-light">HEX</li>
		<li class="secondary">HEX</li>
		<li class="secondary-dark">HEX</li>
		<li class="secondary-darker">HEX</li>
	</ul></dd>

	<dt><?php _ex( 'Secondary Gradients', 'styleguide' ); ?></dt>
	<dd class="interaction"><ul>
		<li class="gradient secondary-gradient"><span>HEX</span><span>HEX</span></li>
		<li class="gradient secondary-gradient-light"><span>HEX</span><span>HEX</span></li>
	</ul></dd>

</dl>
