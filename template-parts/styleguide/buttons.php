<?php
/**
 * Styleguide's button section
 *
 * $args stuff is passed in from main styleguide template
 *
 * @since   1.0.0
 * @package oep
 */
?>

<h3><?php _ex( 'Buttons', 'styleguide' ); ?></h3>

<ul class="buttons">

	<li><?php oep_button([
		'text' => _x( 'Regular Button', 'styleguide' ),
		'url'  => '#',
	]); ?></li>

	<li><?php oep_button([
		'text'  => _x( 'Secondary Button', 'styleguide' ),
		'url'   => '#',
		'class' => 'secondary',
	]); ?></li>

	<li><?php oep_button([
		'text'  => _x( 'Icon Button', 'styleguide' ),
		'url'   => '#',
		'icon'  => 'star',
	]); ?></li>

	<li><?php oep_button([
		'text'   => _x( 'Right Icon Button', 'styleguide' ),
		'url'    => '#',
		'icon'   => 'rocket',
		'icon_r' => true,
		'class'  => 'secondary',
	]); ?></li>

	<li><?php oep_button([
		'text'  => _x( 'Ghost Button', 'styleguide' ),
		'url'   => '#',
		'class' => 'ghost',
	]); ?></li>

	<li class="dark"><?php oep_button([
		'text'  => _x( 'Light Ghost Button', 'styleguide' ),
		'url'   => '#',
		'class' => [ 'ghost', 'light' ],
	]); ?></li>


	<li><?php echo do_shortcode(
		'[button url="https://example.com" icon="check" new-tab]' . __( 'Via Shortcode', 'styleguide' ) . '[/button]'
	); ?></li>

</ul>
