<?php
/**
 * Styleguide's typography section
 *
 * $args stuff is passed in from main styleguide template
 *
 * @since   1.0.0
 * @package oep
 */
?>

<h3><?php _ex( 'Typography', 'styleguide' ); ?></h3>

<dl>

	<!-- fonts -->
	<dt><?php _ex( 'Fonts', 'styleguide' ); ?></dt>
	<dd class="fonts">
		<h4><?php echo $font; ?></h4>
		<h4><?php echo $font_alt; ?></h4>
	</dd>


	<!-- headings -->
	<?php for ( $i = 1; $i <= 6; $i++ ) : //$specs = $type[ "h$i" ]; ?>

		<dt><?php printf( '<h%d>H%d</h%d>', $i, $i, $i ); ?></dt>
		<dd><?php printf(
			_x( '<h%d>%dpx %s - %s, LH %s</h%d>', 'styleguide' ),
			$i, 0, $font, 'Weight', 0.0, $i
			// $i, $specs[0], $specs[1], $specs[2], $specs[3], $i // for later
		); ?></dd>

	<?php endfor; ?>


	<!-- paragraph -->
	<dt>P</dt>
	<dd>
		<p><?php printf( _x( '
			%dpx %s - %s, LH %s%s
			Lorem ipsum dolor sit amet, consectetur adipisicing elit,
			sed do eiusmod tempor incididunt ut labore et dolore magna
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation
			ullamco laboris nisi ut aliquip ex ea commodo consequat.
			Duis aute irure dolor in reprehenderit in voluptate velit
			esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
			occaecat cupidatat non proident, sunt in culpa qui officia.
		', 'styleguide' ), 0, $font, 'Weight', 0.0, '<br>' ); ?></p>
	</dd>


	<!-- detail -->
	<dt><?php _ex( 'Detail', 'styleguide' ); ?><br><code>p > small, .detail</code></dt>
	<dd>
		<p><small><?php printf( _x( '
			%dpx %s - %s, LH %s%s
			Lorem ipsum dolor sit amet, consectetur adipisicing elit,
			sed do eiusmod.
		', 'styleguide' ), 0, $font, 'Weight', 0.0, '<br>' ); ?></small></p>
	</dd>


	<!-- ul/ol -->
	<dt>UL / OL</dt>
	<dd class="lists">
		<ul class="bulleted">
			<li><?php printf( '%dpx %s - %s', 0, $font, 'Weight', 0.0 ); ?><ul>
				<li><?php printf( _x( '%dpx Line Height', 'styleguide' ), 0.0 ); ?></li>
			</ul></li>
			<li><?php _ex( 'List item number 2', 'styleguide' ); ?></li>
			<li><?php _ex( 'List item number 3 with a lot of extra text so it breaks into two lines or more though', 'styleguide' ); ?></li>
			<li><?php _ex( 'List item number 4', 'styleguide' ); ?></li>
			<li><?php printf(
				_x( 'List item number 5 %swith an inline link%s', 'styleguide' ),
				'<a href="#">', '</a>'
			); ?></li>
		</ul>

		<ol class="numbered">
			<li><?php printf( '%dpx %s - %s', 0, $font, 'Weight', 0.0 ); ?><ol>
				<li><?php printf( _x( '%dpx Line Height', 'styleguide' ), 0.0 ); ?></li>
			</ol></li>
			<li><?php _ex( 'List item number 2', 'styleguide' ); ?></li>
			<li><?php _ex( 'List item number 3 with a lot of extra text so it breaks into two lines or more though', 'styleguide' ); ?></li>
			<li><?php _ex( 'List item number 4', 'styleguide' ); ?></li>
			<li><?php printf(
				_x( 'List item number 5 %swith an inline link%s', 'styleguide' ),
				'<a href="#">', '</a>'
			); ?></li>
		</ol>
	</dd>


	<!-- inline link -->
	<dt><?php _ex( 'Links', 'styleguide' ); ?></dt>
	<dd><p>
		<a href="#"><?php printf( _x( '%dpx %s - %s', 'styleguide' ), 0.0, $font, 'Weight' ); ?></a>
	</p></dd>


	<!-- blockquote -->
	<dt><?php _ex( 'Blockquote', 'styleguide' ); ?></dt>
	<dd>
		<blockquote><?php printf(
			_x( '%s %dpx - %s line height %s, and padding 00px. Style for blockquote sections. Lorem ipsum dolor. YOLO', 'styleguide' ),
			$font, 0, 'Weight', 0.0
		); ?></blockquote>
	</dd>

</dl>
