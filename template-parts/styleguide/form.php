<?php
/**
 * Styleguide's form section
 *
 * $args stuff is passed in from main styleguide template
 *
 * @since   1.0.0
 * @package oep
 */
$do_gravity_form = true;
?>

<h3><?php _ex( 'Form', 'styleguide' ); ?></h3>

<form action="">

	<ul class="fields">

		<!-- text -->
		<li class="one-half">
			<label for="styleguide-text"><?php _ex( 'Text', 'styleguide' ); ?></label>
			<input type="text" name="text" id="styleguide-text" placeholder="<?php _ex( 'Placeholder text', 'styleguide' ); ?>">
		</li>


		<!-- text with error -->
		<li class="one-half error"><!-- GF will be .gfield_error -->
			<label for="styleguide-text-invalidated"><?php _ex( 'Invalidated Text', 'styleguide' ); ?></label>
			<input type="text" name="text-invalidated" id="styleguide-text-invalidated">
			<div class="validation-message"><i class="fas fa-exclamation-triangle"></i> <?php _e( 'This field is required.', 'oep' ); ?></div>
		</li>


		<!-- select -->
		<li>
			<label for="styleguide-dropdown"><?php _ex( 'Dropdown', 'styleguide' ); ?></label>
			<select name="dropdown" id="styleguide-dropdown">
				<option disabled selected><?php  _ex( 'Choose something...' , 'styleguide' ); ?></option>
				<option value="1"><?php _ex( 'Choice number one'   , 'styleguide' ); ?></option>
				<option value="2"><?php _ex( 'Choice number two'   , 'styleguide' ); ?></option>
				<option value="3"><?php _ex( 'Choice number three' , 'styleguide' ); ?></option>
				<option value="4"><?php _ex( 'Choice number four'  , 'styleguide' ); ?></option>
				<option value="5"><?php _ex( 'Choice number five'  , 'styleguide' ); ?></option>
			</select>
		</li>


		<!-- textarea -->
		<li>
			<label for="styleguide-textarea"><?php _ex( 'Textarea', 'styleguide' ); ?></label>
			<textarea name="textarea" id="styleguide-textarea" cols="30" rows="10"></textarea>
		</li>


		<!-- checkboxes -->
		<li class="one-half">
			<label><?php _ex( 'Checkboxes', 'styleguide' ); ?></label>
			<ul>
				<li>
					<input type="checkbox" name="checkbox-1" id="styleguide-checkbox-1" value="Checkbox 1">
					<label for="styleguide-checkbox-1"><?php _ex( 'First Choice', 'styleguide' ); ?></label>
				</li>
				<li>
					<input type="checkbox" name="checkbox-2" id="styleguide-checkbox-2" value="Checkbox 2">
					<label for="styleguide-checkbox-2"><?php _ex( 'Second Choice', 'styleguide' ); ?></label>
				</li>
				<li>
					<input type="checkbox" name="checkbox-3" id="styleguide-checkbox-3" value="Checkbox 3">
					<label for="styleguide-checkbox-3"><?php _ex( 'Third Choice', 'styleguide' ); ?></label>
				</li>
			</ul>
		</li>


		<!-- radios -->
		<li class="one-half">
			<label><?php _ex( 'Radios', 'styleguide' ); ?></label>
			<ul>
				<li>
					<input type="radio" name="radio" id="styleguide-radio-1" value="Radio 1">
					<label for="styleguide-radio-1"><?php _ex( 'First Choice', 'styleguide' ); ?></label>
				</li>
				<li>
					<input type="radio" name="radio" id="styleguide-radio-2" value="Radio 2">
					<label for="styleguide-radio-2"><?php _ex( 'Second Choice', 'styleguide' ); ?></label>
				</li>
				<li>
					<input type="radio" name="radio" id="styleguide-radio-3" value="Radio 3">
					<label for="styleguide-radio-3"><?php _ex( 'Third Choice', 'styleguide' ); ?></label>
				</li>
			</ul>
		</li>

	</ul>


	<!-- submit -->
	<?php oep_button([
		'text'   => __( 'Submit', 'styleguide' ),
		'tag'    => 'button',
		'type'   => 'submit',
		'value'  => 'submit',
		'icon'   => 'check',
		'icon_r' => true,
	]); ?>

</form>


<!-- gravity form -->
<?php if ( $do_gravity_form ) {

	if ( function_exists( 'gravity_form' ) ) {
		echo '<h3>' . _x( 'Gravity Form', 'styleguide' ) . '</h3>';
		gravity_form( 1, false, false );
	} else {
		echo '<h3>' . _x( 'Enable Gravity Forms Plugin For Preview', 'styleguide' ) . '</h3>';
	}
}
