<?php
/**
 * Archive's featured posts text/image loop
 *
 * @since   1.2.0
 * @package oep
 */
global $is_featured;
$is_featured = true;
?>

<section class="featured">

	<?php while ( $featured->have_posts() ) : $featured->the_post();
		get_template_part( 'template-parts/content', get_post_type() );
	endwhile; ?>

</section>

<?php wp_reset_postdata(); $is_featured = false;
