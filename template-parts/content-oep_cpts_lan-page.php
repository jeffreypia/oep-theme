<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
?>
<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >

    <div class="entry-content">
		<?php if ( is_front_page() ) {
//			echo do_blocks(get_the_content('','', get_the_ID()));
			$blocks = parse_blocks( $post->post_content );

			$new_content = '';
			foreach ( $blocks as $block ) {
				$new_content .= render_block( $block );
			}
			echo apply_filters( 'the_content', $new_content );
		} else {

			the_content( sprintf(
			/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'oep' ), [ 'span' => [ 'class' => [] ] ] ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( [
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'oep' ),
				'after'  => '</div>',
			] );
		} ?>

    </div><!-- .entry-content -->

</article><!-- #post-## -->
