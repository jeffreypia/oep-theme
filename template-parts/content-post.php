<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
?>
<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >

	<div class="entry-content">
		<?php if ( ! is_single() && get_option( 'rss_use_excerpt' ) ) {
			the_excerpt();
		}

		else {

			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'oep' ), [ 'span' => [ 'class' => [] ] ] ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			));

			wp_link_pages([
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'oep' ),
				'after'  => '</div>',
			]);
		} ?>

	</div><!-- .entry-content -->


	<footer class="entry-footer">
		<?php if ( is_single() ) oep_entry_footer(); ?>
		<?php oep_posted_by('footer') ?>
	</footer>

</article><!-- #post-## -->
