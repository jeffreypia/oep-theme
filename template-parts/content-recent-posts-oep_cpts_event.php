<?php
/**
 * Template part for displaying events in recent posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */

$event_id = get_the_ID();
$bg_image = get_post_thumbnail_id( $event_id );
$date = get_field( 'start_date', $event_id ) ? new DateTime(get_field( 'start_date', $event_id )) : new DateTime();
?>
<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >
    <a class="post_link" href="<?php echo get_the_permalink( $event_id ); ?>"></a>
    <div class="wrap">

        <div class="event_date event_date_large">
            <strong><?php echo date_format( $date, 'd' ); ?></strong>
			<?php echo date_format( $date, 'M' ); ?>
        </div>
        <div class="event_details">
            <!-- title -->
            <h4><?php echo get_the_title( $event_id ); ?></h4>
            <a href="<?php echo get_permalink($event_id); ?>"><span class="event_cta"><?php _e('View Event Details') ?></span></a>
        </div>
    </div>
    <!-- background image -->
	<div class="post_thumbnail-wrap">
        <?php oep_cover_image( oep_get_bg_image( $bg_image ?? null ) ?: OEP_GLOBAL_BANNER, 'banner' ); ?>
    </div>
</article>