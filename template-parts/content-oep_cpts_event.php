<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */

$date = get_field('start_date');
$address = get_field('address');
$datetime = get_field('date_time');
$button_link = get_field('button_link');
$button_text = get_field('button_text');
?>
<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >

	<header class="entry-header">
		<!--Date & Author-->
		<div class="entry-meta">
			<ul class="event_details">
				<li class="event_detail --address"><?php echo $address ?></li>
				<li class="event_detail --date"><?php echo $datetime; ?></li>
			</ul>

			<?php
			if( $button_link ) :
				oep_button([
					'text' 		=> $button_text ?: __( 'Register for this event', 'oep' ),
					'url'  		=> $button_link,
					'new_tab'	=> 'new-tab',
					'class'		=> 'event_registration'
				]);
			endif;
			?>
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php if ( ! is_single() && get_option( 'rss_use_excerpt' ) ) {
			the_excerpt();
		}

		else {

//			the_post_thumbnail( 'large' );

			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'oep' ), [ 'span' => [ 'class' => [] ] ] ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			));

			wp_link_pages([
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'oep' ),
				'after'  => '</div>',
			]);
		} ?>

	</div><!-- .entry-content -->

    <footer class="entry-footer">
		<div class="entry-meta">
			<?php
				//echo get_the_tag_list('<dl class="tag-list"><dt class="tag-list_title">' . __('Tags') . '</dt><dd class="tag-list_tag">','</dd><dd class="tag-list_tag">','</dd></dl>');
			?>
			<div class="social-share">
				<h5 class="social-share_title"><?php _e('Share Event'); ?></h5>
				<?php render_social_sharing_icons(); ?>
			</div><!-- .social-share -->
		</div><!-- .entry-meta -->
    </footer>

</article><!-- #post-## -->
