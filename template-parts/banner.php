<?php
/**
 * Page Banner
 *
 * @since   1.0.0
 * @package oep
 */

extract( $meta );
$is_event_single = is_singular( OEP_EVENT_KEY );
?>

<header class="banner">

	<!-- add to any -->
	<?php if ( function_exists( 'render_social_sharing_icons' ) && is_single() && ! $is_event_single ) : ?>
		<span class="mobile"><?php _e( 'Share Story', 'oep' ); ?></span>
		<?php render_social_sharing_icons(); ?>
	<?php endif; ?>

	<div class="wrap">

		<?php if ( $is_event_single ) : $date = new DateTime( get_field( 'start_date' ) ); ?>
			<div class="event_date event_date_large">
				<strong><?php echo date_format( $date, 'd' ); ?></strong>
				<?php echo date_format( $date, 'M' ); ?>
			</div>
		<?php endif; ?>

		<!-- term pulls, title, subtitle, event date -->
		<div class="title-wrap">

			<?php is_single() ? oep_get_pills() : null; ?>

			<h1><?php echo ! empty( $title ) ? $title : get_the_title();  ?></h1>

			<?php if ( ! empty( $subtitle ) ) : ?>
				<h2><?php echo $subtitle; ?></h2>
			<?php endif; ?>

		</div>

		<?php if ( is_single() && ! $is_event_single ) : ?>
			<span class="entry-date"><?php echo get_the_date( 'M j.Y' ); ?></span>
		<?php endif; ?>

		<?php
		/**
		 * @TODO oep_posted_on() should do the conditional rather than doing it in the actual template file
		 */
		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php oep_posted_by(); ?>
			</div>
		<?php endif; ?>

		<!-- button -->
		<?php isset( $button ) ? oep_button( $button ) : null; ?>

	</div>

	<!-- background image -->
	<?php oep_cover_image( oep_get_bg_image( $bg_image ?? null ) ?: OEP_GLOBAL_BANNER, 'banner' ); ?>

</header>
