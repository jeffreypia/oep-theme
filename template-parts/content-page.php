<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="entry-content wrap">
		<?php
			the_content();

			wp_link_pages([
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'oep' ),
				'after'  => '</div>',
			]);
		?>
	</section><!-- .entry-content -->
</article><!-- #post-## -->
