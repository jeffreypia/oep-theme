<?php
/**
 * Template part for displaying posts for good reads.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
global $count;
$post_type = OEP\Common\oep_get_post_type($post->ID);
?>

<section class="good-reads">

	<div class="wrap">

		<div class="text">
			<h3><?php echo _e('Good Reads'); ?></h3>
			<p><?php echo _e("While you're waiting for your first news update, be sure to check out some of our featured articles below."); ?></p>
		</div>

		<div class="results">

			<?php
				$args = array(
					'posts_per_page' => 8,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'post_type' => 'post',
				);

				$recent_posts = new WP_Query( $args );
				/* Start the Loop */
				while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
				?>

					<article rel="blog-post">
						<!--one click to rule them all //-->
						<a href="<?php the_permalink(); ?>" role="link"></a>

						<figure>
							<div class="post_thumbnail-wrap">
								<?php echo get_the_post_thumbnail(); ?>
							</div>
						</figure>

						<div>
							<?php
							global $count;
							oep_get_pills(
								(isset($count) && $count > 1 && $count < 5) ? 'ghosts' : 'solid',
								(get_query_var( 'taxonomy' )) ? array( get_query_var( 'taxonomy' ) ) : null
							);
							?>

							<span class="screen-reader-text"> <?php the_title(); ?> </span>

						</div><!-- .entry-content -->
						<header class="entry-header">
							<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
						</header><!-- .entry-header -->


						<footer class="entry-footer">
							<?php oep_posted_by('tile'); ?>
							<?php oep_posted_on(); ?>
						</footer>

					</article><!-- #post-## -->

				<?php endwhile;
				wp_reset_postdata();?>

		</div>

	</div>
</section>
