<?php
/**
 * Page Banner
 *
 * @since   1.0.0
 * @package oep
 */
extract( $meta );
?>

<section class="error">

		<div class="message">
			<div class="wrap">
				<h2><?php echo $error_message ?? "It appears that the page you are looking for doesn't exist." ?></h2>
				<p><?php echo $suggestion ?? "Search for another page or navigate to our suggested links below."; ?></p>

				<div class="search">
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>

		<div class="suggested-links">
			<div class="wrap">
				<h2><?php _e('Recommended Links:'); ?></h2>
				<?php wp_nav_menu([
					'theme_location' => 'error',
					'menu_id'        => 'error-menu',
					'depth'          => 2,
					'fallback_cb'    => false,
				]); ?>
			</div>

		</div>
	</div>
</section>
