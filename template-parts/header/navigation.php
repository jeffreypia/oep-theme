<?php
/**
 * Header navigation
 *
 * @todo we will ultimately need to handle multiple menus here as well as a host
 *       of other elements, so the menu_id should be set dynamically using a
 *       customizer option possibly in tandem with a url check. Will probably
 *       need a new menu location too.
 *
 * @since   1.0.0
 * @package oep
 */
?>

<nav>
	<?php wp_nav_menu([
		'theme_location' => 'primary',
		'depth'          => 2,
		'fallback_cb'    => false,
	]); ?>
</nav>
