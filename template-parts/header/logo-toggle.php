<?php
/**
 * Header "top" with logo and mobile nav toggle
 *
 * @since   1.0.0
 * @package oep
 */
?>

<div class="logo-toggle">

    <!-- mobile navigation toggle -->
    <button id="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'oep' ); ?></button>

	<!-- site logo -->
	<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		<?php oep_svg( 'logo' ); ?>
		<p><?php bloginfo( 'name' ); ?></p>
		<p><?php bloginfo( 'description', 'display' ); ?></p>
	</a>

    <!-- search toggle -->
    <button id="search-toggle" class="search-toggle" aria-controls="search-form" aria-expanded="false">
        <span class="screen-reader-text"><?php _e( 'Toggle Search Menu', 'oep' ); ?></span>
        <i class="fas fa-search"></i>
    </button>

</div>
