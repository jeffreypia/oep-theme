<?php
/**
 * Pre-menu
 *
 * @since   1.0.0
 * @package oep
 */

if ( ! has_nav_menu( 'off-canvas' ) ) {
	return;
}
?>

<section class="pre-menu">

	<div id="hamburger-menu" class="hamburger-menu">
		<div class="hamburger">
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>

	<div class="social-weather">
		<?php Oep_Socials::icon_nav(); ?>
		<?php do_action('weather'); ?>
	</div>

</section>

<div class="nav-overlay"></div>
