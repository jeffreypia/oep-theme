<?php
/**
 * Sidebar menu that slides in when #menu-toggle is clicked
 *
 * @todo we will ultimately need to handle multiple menus here as well as a host
 * of other elements, so the menu_id should be set dynamically using a
 * customizer option possibly in tandem with a url check. Will probably need a
 * new menu location too.
 *
 * @since   1.0.0
 * @package oep
 */

if ( ! has_nav_menu( 'off-canvas' ) ) {
	return;
}
?>

<div id="menu-sidebar" class="menu-sidebar">
	<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		<?php oep_svg( 'OEP_Horiz_CMYK' ); ?>
	</a>

	<?php wp_nav_menu([
		'theme_location' => 'off-canvas',
		'depth'          => 2,
	]); ?>

	<button id="menu-sidebar_close" class="menu-sidebar_close"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
