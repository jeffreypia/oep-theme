<?php
/**
 * Sample Home page "services" template part
 *
 * @since   1.0.0
 * @package oep
 */
extract( $meta );
?>

<section id="home-services">
    <div class="wrap">

        <!-- title -->
		<?php echo $title ? "<h2>$title</h2>" : ''; ?>

        <!-- body -->
		<?php isset( $body ) ? oep_entry_content( $body ) : ''; ?>

        <!-- button -->
		<?php isset( $button ) ? oep_button( $button ) : ''; ?>

    </div>
</section>
