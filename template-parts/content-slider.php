<?php
/**
 * Template part for sliders.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
?>


<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >
    <div class="wrap">
        <div>
			<?php
			global $count;
			oep_get_pills(
				'solid'
			);
			?>

        </div>
        <header class="entry-header">
            <h1 class="entry-title"><?php echo $post->post_title; ?></h1>
            <p class="detail"><?php echo has_excerpt(get_the_ID()) ? get_the_excerpt() : substr(get_the_excerpt(),0,strpos(get_the_excerpt(),'.')+1); ?></p>
            <a href="<?php echo get_the_permalink(); ?>" class="more-link"><?php _e("Read Story", "oep"); ?></a>
        </header>
    </div>
    <div class="bg-image">
        <!-- background image -->
	    <?php oep_cover_image( oep_get_bg_image( $bg_image ?? null ) ?: OEP_GLOBAL_BANNER, 'banner' ); ?>
    </div>
</article>


