<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
?>

<div class="social-share">
	<?php render_social_sharing_icons(); ?>
</div>

<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class(); ?> >

	<header class="entry-header">

		<!--Tag Pills-->
		<?php oep_get_pills(); ?>

		<!-- Content Type -->
		<div class="content-type">
			<i class="fas fa-video"></i>
			<p><?php _e('Video', 'oep'); ?></p>
		</div>

		<!--Title-->
		<h1><?php echo get_the_title( get_the_ID() ); ?></h1>

		<!--Date & Author-->
		<div class="entry-meta">

			<p class="date"> <?php echo date_format( new DateTime($post->post_date), 'M j.Y' ); ?> </p>
			<?php oep_posted_by( 'video' ); ?>

		</div>

	</header><!-- .entry-header -->


	<div class="entry-content">

		<?php if ( ! is_single() && get_option( 'rss_use_excerpt' ) ) {
			the_excerpt();
		}

		else {

//			the_post_thumbnail( 'large' );

			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'oep' ), [ 'span' => [ 'class' => [] ] ] ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			));

			wp_link_pages([
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'oep' ),
				'after'  => '</div>',
			]);
		} ?>

	</div><!-- .entry-content -->

    <footer class="entry-footer">
		<?php if ( is_single() ) oep_entry_footer(); ?>
		<?php oep_posted_by('footer') ?>
    </footer>

</article><!-- #post-## -->
