<?php
/**
 * Template part for recent posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */

global $count;
?>


<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class($count !== 1 ?: ['featured']); ?> >

    <figure>
	    <?php oep_cover_image( oep_get_bg_image( $bg_image ?? null ) ?: OEP_GLOBAL_BANNER, 'large' ); ?>
    </figure>
    <div>
		<?php
		oep_get_pills(
			'solid'
		);
		?>

    </div>
    <header class="entry-header">
        <h4 class="entry-title"><?php echo $post->post_title; ?></h4>
        <?php the_excerpt(); ?>
    </header>
    <footer class="entry-footer">
		<?php oep_posted_by( 'tile' ); ?> <?php oep_posted_on(); ?>
    </footer>
    <a class="post-link" href="<?php echo get_the_permalink(); ?>"></a>
</article>


