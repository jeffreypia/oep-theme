<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
global $count;
$post_type = OEP\Common\oep_get_post_type($post->ID);
?>

<article id="post-<?php the_ID(); ?>" rel="blog-post" <?php post_class( 1 === $count ? '-primary' : '' ); ?> >
    <!--one click to rule them all //-->
    <a href="<?php echo esc_url( get_permalink()); ?>" role="link"></a>

    <figure>
        <div class="post_thumbnail-wrap video">
            <?php
//            the_post_thumbnail('thumbnail');
            do_action( 'media_thumbnail', $post);
            ?>
        </div>
    </figure>
    <div>

		<?php
        global $count;
		oep_get_pills(
            (isset($count) && $count > 1 && $count < 5) ? 'ghosts' : 'solid',
            (get_query_var( 'taxonomy' )) ? array( get_query_var( 'taxonomy' ) ) : null
        );


		the_title( '<span class="screen-reader-text">"', '"</span>', true );
		?>

    </div><!-- .entry-content -->
    <header class="entry-header">

		<?php
			the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a> ' . $post_type . '</h4>' );

		?>

    </header><!-- .entry-header -->


    <footer class="entry-footer">
		<?php oep_posted_by( 'tile' ); ?><?php oep_posted_on(); ?>
    </footer>

</article><!-- #post-## -->
