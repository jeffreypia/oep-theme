<?php
/**
 * Template part for displaying event posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */
global $count;
$post_type = OEP\Common\oep_get_post_type($post->ID);
$date = new DateTime( get_field('start_date') );
?>

<article id="post-<?php the_ID(); ?>" rel="event-post" class="post --event">
    <!--one click to rule them all //-->
    <a class="post_link" href="<?php echo esc_url( get_permalink()); ?>" role="link"></a>

    <figure class="post_thumbnail">
        <div class="post_thumbnail-wrap">
            <?php the_post_thumbnail('thumbnail'); ?>
        </div>

        <figcaption>
            <div class="event_date event_date_large">
                <strong><?php echo date_format( $date, 'd' ); ?></strong>
                <?php echo date_format( $date, 'M' ); ?>
            </div>

            <div class="event_details">
                <h4 class="post_title"><?php echo $post->post_title; ?></h4>
                <span>View Event Details</span>
            </div>
        </figcaption>
    </figure>
    
</article><!-- #post-## -->
