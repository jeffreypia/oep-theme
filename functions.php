<?php
/**
 * oep functions and definitions.
 *
 * @since   1.0.0
 * @package oep
 */

/**
 * Define stuff
 */
define( 'OEP_THEME_VERSION' , '1.2.0'                                   );
define( 'OEP_DIR'           , get_template_directory()                  );
define( 'OEP_DIR_URI'       , get_template_directory_uri()              );
define( 'OEP_CHILD_DIR'     , get_stylesheet_directory()                );
define( 'OEP_GAPI'          , 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' ); // also update in custom plugin if needed!

/**
 * global profile image when none provided
 */
define('OEP_GLOBAL_THUMBNAIL', wp_get_attachment_image( get_theme_mod( 'profile_image' ) ) ?? null);

/**
 * Get includes - theme-only functionality
 */
function oep_load_theme_includes() {
	$includes = [
		'functions',
		'setup',
		'class-scripts',
		'class-404',
		'class-acf',
		'class-blog',
		'class-button',
		'class-customizer',
		'class-contacts',
		'class-socials',
		'class-gravity-forms',
		'class-archive-features',
		'class-archive-blocks',
		'image-sizes',
		'excerpts',
		'component/cover-image',
		'component/template-tags',
	];
	foreach ( $includes as $include ) {
		require OEP_DIR . '/includes/' . $include . '.php';
	}


	/**
	 * Add contact info customizer controls
	 *
	 */
	Oep_Contacts::add_group( __( 'Orlando' , 'oep' ) );


	/**
	 * Add socials (also registers customizer stuff)
	 */
	Oep_Socials::add_profiles([
		__( 'Facebook'    , 'oep' ) => 'facebook-f',
		__( 'Twitter'     , 'oep' ) => 'twitter',
		__( 'Instagram'   , 'oep' ) => 'instagram',
		__( 'LinkedIn'    , 'oep' ) => 'linkedin-in',
		__( 'YouTube'     , 'oep' ) => 'youtube',
	]);

	/**
	 * Add theme settings
	 */
	Oep_Customizer::add_section( __( 'Theme Settings', 'oep' ), [

		'banner_image'		=> [
			'label' 		=> __( 'Upload your Banner Image', 'oep' ),
			'type' 			=> 'media'
		],
		'profile_image'     => [
			'label' 		=> __( 'Upload your Profile Image', 'oep' ),
			'type' 			=> 'media'
		],
	], 30, 'theme_images' );
}
add_action( 'after_setup_theme', 'oep_load_theme_includes' );


/**
 * Set user agent so we can get_file_contents and stuff
 */
ini_set( 'user_agent', "oepAgent" );


/**
 * Order Event post type by meta key value
 */
function oep_sort_events( $query ) {
	// do not modify queries in the admin
	if( is_admin() ) {
		return $query;
	}

	// only modify queries for 'event' post type
	if( isset($query->query_vars['post_type']) && OEP_EVENT_KEY === $query->query_vars['post_type'] ) {

		$query->set('orderby', 'meta_value');
		$query->set('meta_key', 'start_date');
		$query->set('order', 'ASC');

	}
	return $query;
}
add_action('pre_get_posts', 'oep_sort_events', 10, 1);

/**
 * Filter Event post type by option selected in filter drop down
 */
function oep_filter_events( $query ) {
	// do not modify queries in the admin
	if( is_admin() || !$query->is_main_query() ) {
		return $query;
	}

	// only modify queries for 'event' post type
	if( is_post_type_archive( OEP_EVENT_KEY ) ) {
		$today = date("Ymd"); // Set start date to today
		$start_date = strtotime($today); // Convert to date object
		$filter = (isset($_GET) && isset($_GET["filterby"])) ? urldecode($_GET["filterby"]) : NULL; // Get string to calculate end date
		$end = strtotime($filter, $start_date); // Convert to date object
		$start_date = date('Ymd', $start_date); // Calculate end date
		$end_date = date('Ymd', $end); // Calculate end date

		if($filter){
			// Run query
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'start_date');
			$query->set('order', 'ASC');
			$query->set('meta_query', array(
				array(
					'key' 		=> 'start_date',
					'value' 	=> array($start_date, $end_date),
					'compare' 	=> 'BETWEEN',
					'type' 		=> 'DATE'
				)
			));

		}
	}
	return $query;
}
add_action('pre_get_posts', 'oep_filter_events', 10, 1);

/**
 * Registering custob query vars
 */
function custom_query_vars_filter($vars) {
	$vars[] .= 'filterby';
	return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter' );

/**
 * Remove paragraph tags from tax description
 **/
remove_filter('term_description','wpautop');
