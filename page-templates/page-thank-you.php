<?php
/**
 * Thank You Page Template
 *
 * Template Name: Thank You
 *
 * @since   1.0.0
 * @package oep
 */

 get_header(); ?>

	<?php oep_section( 'banner' ); ?>

	<main class="default" role="main">

		<?php
		if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'good-reads' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</main><!-- #main -->

<?php get_footer();
