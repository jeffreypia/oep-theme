<?php
/**
 * Styleguide template
 *
 * Template Name: Styleguide
 *
 * @since   1.0.0
 * @package oep
 */
$args = [
	'font'     => _x( 'Modelica' , 'styleguide' ),
	'font_alt' => _x( 'Merriweather'   , 'styleguide' ),
];

get_header(); ?>

	<?php //oep_section( 'banner' ); ?>

	<main class="default wrap" role="main">

		<section><?php
			oep_template_part( 'styleguide/typography' , null, $args );
			oep_template_part( 'styleguide/icons'      , null, $args );
			oep_template_part( 'styleguide/buttons'    , null, $args );
		?></section>

		<section><?php
			oep_template_part( 'styleguide/colors'     , null, $args );
			oep_template_part( 'styleguide/form'       , null, $args );
			oep_template_part( 'styleguide/pagination' , null, $args );
		?></section>

	</main>

<?php get_footer();
