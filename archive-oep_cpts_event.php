<?php
/**
 * The template for displaying Event listing page.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 * @package oep
 */

get_header();
$filterby = urldecode(get_query_var("filterby"));
?>

    <main id="main" class="site-main wrap" role="main">

    <?php
    if ( have_posts() ) : ?>

        <header class="page-header">
            <h1 class="page-title"><?php _e('Upcoming Events'); ?></h1>
            <form method="GET" action="" id="eventFilter" class="event-filter">

                <div class="wrap">
                    <label for="event-filter_select">
                        <span class="">Event filter:</span>
                        <select id="event-filter_select" name="filterby">
                            <option <?php echo !$filterby ? 'selected' : ''; ?> value="0">All upcoming events</option>
                            <option <?php echo 'today' === $filterby ? 'selected' : ''; ?> value="today">Today</option>
                            <option <?php echo ' 1 week' === $filterby ? 'selected' : ''; ?> value="+1 week">This week</option>
                            <option <?php echo 'last day of this month' === $filterby ? 'selected' : ''; ?> value="last day of this month">This month</option>
                            <option <?php echo ' 1 year' === $filterby ? 'selected' : ''; ?> value="+1 year">This year</option>
                        </select>
                    </label>
                </div><!-- .wrap -->
            </form>
        </header><!-- .page-header -->

        <div class="posts --events">
            <?php
            /* Start the Loop */
            while ( have_posts() ) : the_post();
                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
                get_template_part( 'template-parts/content-grid', OEP_EVENT_KEY . '-overlay' );

            endwhile;
            ?>
       </div><!-- .--events -->

        <?php
        the_posts_navigation();

    else :

        get_template_part( 'template-parts/content-none', OEP_EVENT_KEY );

    endif; ?>

    </main><!-- #main -->
    <section class="additional-items wrap">
        <?php do_action('trending_stories'); ?>
    </section>

<?php get_footer();
