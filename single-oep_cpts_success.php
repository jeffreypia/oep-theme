<?php
/**
 * Success stories single
 */

get_header(); ?>

	<main class="wrap" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content',  get_post_type() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		<aside>
			<?php
				do_action('related_post');
				do_action('newsletter');
			    do_action('featured_post');
			    wp_reset_postdata();
			?>
		</aside>

	</main>
    <section class="additional-items wrap">
		<?php do_action('recommended_stories'); ?>
		<?php do_action('trending_stories'); ?>
    </section>


<?php get_footer();
