/**
 * Local vars for gulpfile.babel.js
 *
 * @package staypromo
 */
var os = require('os');
var homeDir = os.homedir();

const localGulpConfig = {
    bsURL    : 'https://localurl.test/',
    bsOpen   : false,
    keyPath  : homeDir + '/DZ/mamp-certificates/staypromo.test.key',
    certPath : homeDir + '/DZ/mamp-certificates/staypromo.test.key',
}
export { localGulpConfig };