<?php
/**
 * Landing page single
 */

get_header();
?>
	<main role="main">
		<?php do_action("oep_header"); ?>

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>

	</main>

<?php get_footer();
