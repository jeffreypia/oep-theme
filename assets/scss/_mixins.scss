/**
 * Mixins
 *
 * @since   1.0.0
 * @package oep
 */

/**
 * Generic wrapper
 */
@mixin wrap( $max-width : $wrap-max, $width : $wrap-width ) {
	margin-left: auto;
	margin-right: auto;
	width: $width;
	max-width: $max-width;
}


/**
 * Object-fit "cover"
 */
@mixin cover-image {
	object-fit: cover;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}


/**
 * Centered block
 */
@mixin center-block {
	display: block;
	margin-left: auto;
	margin-right: auto;
}


/**
 * Centered element trick!
 */
@mixin absolute-center( $stretch: false, $extra-transforms: "", $top: 50%, $left: 50% ) {
	position: absolute;
	top: $top;
	left: $left;
	width: auto;
	max-width: 1000%;
	height: auto;
	transform: translateX( -50% ) translateY( -50% ) unquote( $extra-transforms );

	@if $stretch {
		min-width: 100%;
		min-height: 100%;
	}
}


/**
 * Make a flex "row"
 */
@mixin flex-row( $align-center: false, $inline-flex: false ) {

	@if $inline-flex {
		display: inline-flex;
	} @else {
		display: flex;
	}

	flex-flow: row nowrap;

	@if $align-center {
		align-items: center;
	}
}


/**
 * Font smoothing
 */
@mixin font-smoothing {
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}


/**
 * Kill font smoothing!
 */
@mixin no-font-smoothing {
	-webkit-font-smoothing: auto;
	-moz-osx-font-smoothing: auto;
}


/**
 * Inline text link
 */
@mixin inline-link {
	color: $primary;
	border-bottom: 1px solid transparent;
	transition-property: color, border-bottom-color;
	@include transition-ease-in-out;

	&:hover {
		color: $primary-light;
		border-bottom-color: $primary-light;
	}
}


/**
 * Styled list
 */
@mixin list {

	li {
		position: relative;
		margin: bsu(1/2) 0;
		padding-left: bsu(1);

		// counter / icons
		&:before, &:after,
		> span > .svg-inline--fa {
			position: absolute;
			left: 0;
		}
	}

	// nested
	ul, ol {
		margin-top: 0;
	}

	a {
		@include inline-link;
	}
}


/**
 * Bulleted list
 *
 * Icons inserted with js/foot/list-icons.js
 */
@mixin bulleted-list {
	li {
		&:before, &:after,
		> span > .svg-inline--fa {
			width: 4px;
			height: 4px;
			top: rem-calc(16);

			@include breakpoint( xlarge ) {
				top: rem-calc(18);
			}
		}
	}
}


/**
 * Numbered list
 */
@mixin numbered-list {

	&, ol {
		counter-reset: section;
	}

	li:before {
		counter-increment: section;
		content: counters( section, "." ) ".";
	}

	// nested
	ol li:before {
		font-size: 0.9rem;
	}
}


/**
 * Remove bottom margin if last child
 */
@mixin no-bottom-if-last {

	&:last-child {
		margin-bottom: 0;
	}
}


/**
 * Remove top margin if first child
 */
@mixin no-top-if-first {

	&:first-child {
		margin-top: 0;
	}
}

/**
 * Remove left margin if first child
 */
@mixin no-left-if-first {

	&:first-child {
		margin-left: 0;
	}
}

/**
 * Remove right margin if last child
 */
@mixin no-right-if-last {

	&:last-child {
		margin-right: 0;
	}
}


/**
 * WYSIWYG content format
 */
@mixin wysiwyg {

	// base spacing
	h1, h2, h3, h4, h5, h6,
	> ul, > ol,
	form,
	blockquote {
		margin-top: bsu(1);
	}

	[role="button"] {
		margin: bsu(1) bsu(1/2) 0 0;
	}

	// no space if at the beginning or end
	> h1, > h2, > h3, > h4, > h5, > h6,
	> p,
	> ol, > ul,
	blockquote, blockquote p {
		@include no-bottom-if-last;
		@include no-top-if-first;
	}

	// lists
	> ul, > ol {
		@include list;
	}
	> ul {
		@include bulleted-list;
	}
	> ol {
		@include numbered-list;
	}

} // wysiwyg


/**
 * Responsive embed container
 */
@mixin responsive-embed-container {
	position: relative;
	padding-bottom: 56.25%;
	width: 100%;
	max-width: 100%;
	height: auto;
	max-height: 440px;
	overflow: hidden;

	iframe, object, embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
}


/**
 * Fluid Property!
 */
@mixin fp( $property, $min, $max, $start: 320, $end: 1280, $clip: true, $clipAtStart: true, $clipAtEnd: true ) {

	$multiplier: ($max - $min) / ($end - $start) * 100;
	$adder: ($min * $end - $max * $start) / ($end - $start);
	$formula: calc(#{$multiplier + 0vw} + #{$adder + 0px});

	@if $clip and $clipAtStart {
		@media (max-width: #{$start + 0px}) {
			#{$property}: $min + 0px;
		}
	}
	@if $clip and $clipAtEnd {
		@media (min-width: #{$end + 0px}) {
			#{$property}: $max + 0px;
		}
	}

	#{$property}: $formula;

} // fp()

@function fp-calc( $min, $max, $start: 320, $end: 1280 ) {

	$multiplier: ($max - $min) / ($end - $start) * 100;
	$adder: ($min * $end - $max * $start) / ($end - $start);
	$formula: calc(#{$multiplier + 0vw} + #{$adder + 0px});

	@return $formula;
}


/**
 * Overlay
 */
@mixin overlay( $z-index: 0, $top: 0, $right: 0, $bottom: 0, $left: 0 ) {
	content: '';
	display: block;
	position: absolute;
	z-index: $z-index;
	top: $top;
	right: $right;
	bottom: $bottom;
	left: $left;
}


/**
 * Generic transition
 */
@mixin transition-ease-in-out( $speed: 200ms ) {
	transition-timing-function: ease-in-out;
	transition-duration: $speed;
}


/**
 * Clearfix
 */
@mixin clearfix {

	&:before {
		content: " ";
		display: table;
	}

	&:after {
		clear: both;
		content: " ";
		display: table;
	}
} // clearfix()

/**
 * Pills mixin
 */

@mixin pills {
	> ul,
	ul.pills {
		margin: 0;
		padding: 0;
		list-style-type: none;
		font-size: 1em;
		line-height: 2.5;
		display: flex;
		flex-flow: row wrap;
		align-items: center;
		justify-content: flex-start;

		li {
			margin: rem-calc(5);
			font-family: "Modelica Extra Bold";
			font-weight: bold;
			font-size: 0.625em;
			line-height: 1;
			text-transform: uppercase;
			border-radius: rem-calc(11);


			a {
				display: block;
				padding: rem-calc(5) rem-calc(10);
				text-decoration: none;
				font-family: "Modelica Medium";
				font-style: normal;

				&:hover {
					text-decoration: none;
				}
			}
		}

		&.ghosts {
			li {
				background-color: transparent;
			}
		}

		.post-thumbnail & {
			position: absolute;
			left: 15px;
			top: 15px;
		}
	}
}

/**
 * featured tile mixin
 */
@mixin featured() {
	color: $white;
	padding: rem-calc(10) rem-calc(10) rem-calc(10) rem-calc(15);

	@include breakpoint(large) {
		justify-content: flex-start;
	}

	&:before {
		@include overlay();
		content: "";
		border-radius: $border-radius;
		background: linear-gradient(to top, rgba($black, 0.8) 0, rgba($black, 0.2) 50%, rgba($black, 0.2) 100%);
	}

	> * {
		position: relative;
		width: auto;
		max-width: none;
	}

	> div {
		z-index: 4;
	}

	header {
		z-index: 1;
		margin: auto 0 rem-calc(10) 0;

		& > span {
			display: none;
		}

		@include breakpoint(xlarge) {
			margin-right: 20%;
		}
	}

	figure {
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		max-width: none;
		z-index: -1;

		img {
			max-height: rem-calc(495);
		}

		.post_thumbnail-wrap {
			&:after {
				@include overlay(1);
				bottom: 0;
				content: "";
				border-radius: $border-radius;
				background: linear-gradient(to top, rgba($black, 1) 0, rgba($black, 0.1) 50%), rgba($black, 0) 100%;
				opacity: 0;
				@include transition-ease-in-out();
			}
		}
	}

	footer {
		display: none;
		z-index: 4;
	}

	> span {
		display: none;
		margin-bottom: .625rem;
		z-index: 4;
		opacity: 0;
		transition-property: opacity;
		@include transition-ease-in-out();
		@include arrow-right-link();

		svg {
			color: white;
		}
	}

	&:hover {
		> span {
			opacity: 1;
		}

		figure {
			div.post_thumbnail-wrap {
				&:after {
					opacity: 1;
				}
			}
		}

		header {
			margin-top: 0;

			@include breakpoint (large){
				order: 2;
			}
		}

		footer {
			display: block;
			z-index: 4;

			@include breakpoint (large){
				margin-top: auto;
				order: 1;
				margin-bottom: -10px;
			}
		}
	}
}

/**
 * Arrow icon w/ white circular border and transparent bg
 */
@mixin arrow-right-link() {
	display: flex;
	align-items: center;
	justify-content: center;
	width: rem-calc(35);
	height: rem-calc(35);
	border: solid $border-width $white;
	border: solid 1.5px $white;
	border-radius: 50%;
	text-align: center;
	background-color: rgba($white, 0.2);
	position: absolute;
	right: rem-calc(20);
	bottom: rem-calc(15);

	svg {
	    color: white;
	}
}

/**
 * Grid stacked on Mobile -> 3-col at large bp
 */
@mixin grid {
    display: flex;

    main {
        margin-top: 0;
        flex-flow: row wrap;
        max-width: 1530px;
        margin-top: 1.875rem;
        padding-top: 1.875rem;

        @include breakpoint(xlarge) {
          margin-top: rem-calc(30);
          padding-top: rem-calc(30);
        }

        &.wrap {
            flex-flow: column wrap;
            max-width: 1530px;
        }
    }

    .posts {
		display: block;
    }

    .post {
        margin: 20px 0 0;

        &:first-child {
        	margin-top: 0;
        }

        & + .post {
        	margin-top: 20px;
        }
    }

	@include breakpoint (medium) {
    	.posts {
    		display: flex;
    		flex-flow: row wrap;
    		justify-content: space-between;
    		margin-left: -10px;
    		margin-right: -10px;
    	}

		.post {
			flex-basis: 50%;
			padding: 0 10px;

			&:nth-child(2) {
				margin-top: 0;
			}

		}

		.post_title {
			white-space: initial;
		}
	}

	@include breakpoint (large) {
    	.post {
    		flex-basis: 33%;

    		&:nth-child(3) {
    			margin-top: 0;
    		}
    	}
    }
}

/***
 * Horizon divider w/ pattern
 */
@mixin horizontal-divider {
    margin-bottom: 30px;
    padding-bottom: 30px;
    position: relative;

    &:before {
        display: block;
        content: '';
        width: 10px;
        height: 45px;
        background: url("../images/detail-repeater-r2.png") repeat-y 0 0;
        position: absolute;
        left: 50%;
        bottom: -30px;
        transform: rotate(90deg) translateX(-50%) translateY(50%);
    }
}

/* mixin for multiline */
@mixin multiLineEllipsis($lineHeight: 1.5em, $lineCount: 3, $bgColor: white){
	@media screen and (min-width: 320px) and (max-width: 1440px){
		overflow: hidden;
		position: relative;
		line-height: $lineHeight;
		max-height: $lineHeight * $lineCount;
		text-align: left;
		padding-right: 1em;
		&:before {
			content: none;
			@include breakpoint(large){
				content: none;
			}
			position: absolute;
			right: 0;
			bottom: 0;
		}
		&:after {
			content: '...';
			position: relative;
			left: 0;
			width: 1em;
			height: 1em;
			margin-top: 0.2em;

			@include breakpoint(large){
				content: '';
				position: absolute;
				right: 0;
				width: 1em;
				height: 1em;
				margin-top: 0.2em;
			}

		}
	}
}
