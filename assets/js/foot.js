/**
 * Add To Any Sticky icons
 *
 * @since   1.0.0
 * @package oep
 */

(function ($) {
  $(document).ready(function () {
    var $nav = $('.a2a_kit');

    if ( $nav.length < 1 ) {
      //no add to any icons here, bounce
      return;
    }

    //create a duplicate for sharing in the footer on mobile devices
    $('.entry-footer').prepend($nav.clone().addClass("content-end")).prepend($('span.mobile').clone().addClass("content-end"));

    //add a class to our original set of icons
    $nav.addClass("in-banner");

    $nav.find('span').each(function () {
      $(this).attr('data-color', $(this).css('backgroundColor'));
      $(this).css('backgroundColor', 'transparent');
    });

    $('#content > header.banner').scrollspy({
      min: $nav.offset().top,
      max: $('footer').offset().top,
      onEnter: function (element, position) {
        $nav.addClass('fixed');
        $nav.find('span').each(function () {
          $(this).css('backgroundColor', $(this).attr('data-color'));
        });
      },
      onLeave: function (element, position) {
        $nav.removeClass('fixed');
        $nav.find('span').each(function () {
          $nav.find('span').css('backgroundColor','transparent');
        });
      }
    });

	//make social share section full color at all times
	$('#content .social-share .a2a_kit').find('span').each(function () {
    $(this).css('backgroundColor', $(this).attr('data-color'));
	});
	
  });
})(jQuery);

/**
 * Ajax Pagination
 * Because Johnny asked nicely
 *
 * @since   1.0.0
 * @package oep
 */

(function ($) {

  var $types = ($('main div.results > div > select').length > 0) ? $('main div.results > div > select') : $('input.types');
  console.log($types);
  var $filters = $('.filters select');

  function find_page_number( element ) {
    if(element.find('span').length > 0) {
      element.find('span').remove();
      return parseInt(element.html());
    } else {
      return 1;
    }
  }

  function ajaxify ( e ){
    e.preventDefault();

    var $header          = $( 'header[role=banner]' );
    var $headerHeight   = $header.outerHeight();
    var $target = $('main .results');
    var $scrollToPosition = 0;

    page = find_page_number( $(this).clone() );
    spinner_url = ajaxpagination.spinner_url;


    $.ajax({
      url: ajaxpagination.ajaxurl,
      type: 'post',
      data: {
        action: 'ajax_pagination',
        query_vars: ajaxpagination.query_vars,
        page: page,
        base_url: ajaxpagination.base_url,
        post_type: $types.val(),
        oep_taxonomies_industries: $('[name=oep_taxonomies_industries]').val(),
        oep_taxonomies_size: $('[name=oep_taxonomies_size]').val(),
        oep_taxonomies_regions: $('[name=oep_taxonomies_regions]').val(),
      },
      beforeSend: function() {
        $scrollToPosition = $target.offset().top;
        $('html, body').animate({
          scrollTop: $scrollToPosition - $headerHeight,
        }, 500);
        $target.toggleClass('hidden');
        $target.find('nav').remove();
        $target.append( '<div class="pagination-overlay" id="loader"><img src="'+ spinner_url +'" /></div>' );
      },
      success: function( html ) {
        $target.find( 'article' ).remove();
        $target.find('nav').remove();
        $('main #loader').remove();
        $target.append( html );
        $target.toggleClass('hidden');

        //do it all over again
        $('.posts-navigation.numbered-pagination ul li a').on( 'click', ajaxify );
      }
    });
  }
  // event handlers
  $('.posts-navigation.numbered-pagination ul li a').on( 'click', ajaxify );
  $types.change( ajaxify );
  $filters.change( ajaxify );
})(jQuery);
/**
 * Add Font Awesome icons to stuff
 *
 * @since   1.0.0
 * @package bigblue
 */

(function($) {
    $(document).ready(function() {

        $('.wp-block-audio').each(function(){
            var $audio = $(this).find('audio');
            var $link = $('<a class="audio-link" href="' + $audio.attr("src") + '">Download Podcast</a>');
            $audio.before($link);
        });

    }); // document.ready
})(jQuery);

/**
 * Filter for Events listing page
 *
 * @since   1.0.0
 * @package oep
 */

(function($) {
    $(document).ready(function() {
        var $form = $("#eventFilter");
        
        $form.find('select').on('change', function(event){
            $form.submit();
        })
    }); // document.ready
})(jQuery);

/**
 * Form interactions
 *
 * @since   1.0.0
 * @package oep
 */
(function($) {

	var $document = $( document );
	$document.ready( init );

	/**
	 * Start setting/binding document-ready stuff
	 *
	 * @since 1.0.0
	 */
	function init() {

		var $body       = $( document.body );
		var $prefixes   = $( '.field-prefix' );
		var $checkboxes = $( '[type="checkbox"], [type="radio"]' );

		$prefixes.each( doPrefixSpace );
		$checkboxes.each( checkCheckboxes );
		$checkboxes.on( 'change', checkCheckboxes );
		$body.on( 'update_checkout', checkCheckboxes );
	}


	/**
	 * Apply field prefix spacing
	 *
	 * @since 1.1.1
	 */
	function doPrefixSpace() {

		var $this = $(this);
		var padding = parseInt( $this.css( 'paddingLeft' ) ) + $this.width() + 1 + 'px';

		$this.next( 'input' ).css( 'paddingLeft', padding );
	}


	/**
	 * See if checkboxes and radios are checked, flagging a parent <label>
	 *
	 * @since 1.0.0
	 */
	function checkCheckboxes() {

		$this = $(this);

		checked = $this.prop( 'checked' );
		$label  = $this.parent( 'label' );

		if ( checked ) {
			$label.addClass( 'input-checked' );
		} else {
			$label.removeClass( 'input-checked' );
		}
	}

})(jQuery);

/**
 * Add Font Awesome icons to stuff
 *
 * @since   1.0.0
 * @package bigblue
 */

(function($) {
	$(document).ready(function() {

		$.each({
			'.entry-content > ul li, ul.bulleted li' : ['circle', 				'fas'],
			// '.wp-block-column ul li'				 : ['circle', 				'fas'],
			'.wp-block-oep-headline-text ul li'		 : ['circle', 				'fas'],
			'.wp-block-oep-event-text ul li'		 : ['circle', 				'fas'],
			'.wp-block-oep-text-text ul li'			 : ['circle', 				'fas'],
			'.editor-rich-text ul li' 				 : ['circle', 				'fas'],
			'.validation_message'                    : ['exclamation-triangle', 'fas'],
			'.ginput_container_date'                 : ['calendar-alt', 		'fas'],
			'.event_detail.--address'				 : ['map-marker-alt', 		'fal'],
			'.event_detail.--date'					 : ['clock', 				'fal'],
			'#menu-global-sidebar .phone-link a'	 : ['phone', 				'fas'],
		}, prependIcon );

		$.each({
			'.entry-content a[target=_blank]'					 	: ['external-link-alt', 'fas'],
			'.tiles.wide.in-content article'						: ['arrow-right', 'fal'],
			'.featured .row article'								: ['arrow-right', 'fal'],
			'.--featured ul.posts li.post'							: ['arrow-right', 'fal'],
			'.--upcoming-events .post_thumbnail'					: ['arrow-right', 'fal'],
			'.--upcoming-events .more-link'							: ['arrow-right', 'fal'],
		    '.oep_cpts_event .post_thumbnail-wrap'      			: ['arrow-right', 'fal'],
			'.recent-posts.has-stories .more-link'								: ['arrow-right', 'fal'],
			'.wp-block-oep-column-sidebar .more-link a'				: ['arrow-right', 'fal'],
			'.wp-block-oep-column-sidebar .recent-posts a.more-link'				: ['arrow-right', 'fal'],
			'.post-type-archive-oep_cpts_event .event_details'		: ['arrow-right', 'fal'],
			'.wp-block-oep-slider .more-link'						: ['arrow-right', 'fal'],
			'header.banner.slider header.entry-header .more-link'	: ['arrow-right', 'fal'],
			'.recent-posts .featured'								: ['arrow-right', 'fal'],
			'.has-stories.has-3-posts article'						: ['arrow-right', 'fal'],
			'.has-stories.has-4-posts article:first-child'			: ['arrow-right', 'fal'],
			'.has-stories.has-6-posts article'						: ['arrow-right', 'fal'],
			'.wp-block-oep-general-text-header .text-only .wp-block-button__link' : ['arrow-right', 'far'],
		}, appendIcon );

		/**
		 * Prepend an icon to a jQuery object
		 *
		 * @param {string} thing selector
		 * @param {string} icon  name of Font Awesome icon (like file-text)
		 * @param {string} collection  of Font Awesome icon (like fas, fab, fal etc)
		 */
		function prependIcon( thing, icon ) {
			$( thing ).addClass('xxx').prepend( '<span class="icon-wrap"><i class="' + icon[1] +' fa-' + icon[0] + '"></i></span>' );
		}

		/**
		 * Append an icon to a jQuery object
		 *
		 * @param {string} thing selector
		 * @param {string} icon  name of Font Awesome icon (like file-text)
		 * @param {string} collection  collection of Font Awesome icon (like fas, fab, fal etc)
		 */
		function appendIcon( thing, icon ) {
			$( thing ).append( '<span class="icon-wrap"><i class="' + icon[1] +' fa-' + icon[0] + '"></i></span>' );
		}

		//resize bulleted lists
		$('ul.bulleted').addClass("fa-ul");
		$('ul.bulleted li span').addClass("fa-li");
		$('ul.bulleted li span i').addClass("fa-xs");

		//recolor heading svgs
		$('div.svg.heading svg').attr('fill', $('div.accent').data('color'));
		$('div.svg.heading svg').attr('style', 'fill: "' + $('div.accent').data('color') + '"');

		$('div.svg.heading-accent svg').each(function(index){
			$( this ).attr('fill', $(this).parent().parent().data('color'))
		});
		// $('div.svg.heading-accent svg').attr('style', 'fill: "' + $('div.accent').data('color') + '"');

	}); // document.ready
})(jQuery);

/**
 * Video Gutenberg blocks
 *
 * Launches video content in a lightbox
 */
( function($) {
	$(document).on('ready', function () {

		/**
		 * Get the poster attribute of a native <video>
		 *
		 * If it's a video block and poster image has been set, the URL should
		 * be in a 'poster' attribute.
		 *
		 * @param  {jQuery} $video
		 * @return {string} poster prop image URL
		 */
		var getPoster = function( $video ) {

			if ( $video[0].hasAttribute( 'poster' ) ) {
				return $video.attr( 'poster' );
			} else {
				return false;
			}
		};


		/**
		 * Grab the ID from YouTube SRC and grab the thumbnail URL for it.
		 *
		 * @see    https://stackoverflow.com/questions/5817505/is-there-any-method-to-get-the-url-without-query-string
		 * @param {string} url
		 */
		var getPosterYouTube = function( url ) {

			if ( ! url ) {
				return;
			}

			var id = url.split( '?' )[0].split( '/' ).pop();
			return 'https://img.youtube.com/vi/' + id + '/maxresdefault.jpg';
		};


		/**
		 * Get Vimeo video thumbnail via API and output it in wrapper
		 *
		 * This is an embed, so extract ID from src attr and try try to
		 * construct an image URL
		 *
		 * @see   https://stackoverflow.com/questions/2916544/parsing-a-vimeo-id-using-javascript
		 * @param {jQuery} $container
		 * @param {string} url
		 */
		var getPosterVimeo = function( $container, url ) {

			var id = url.split('?')[0].split('/').pop();

			$.ajax({
				type: "GET",
				url: 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/' + id,
				contentType: "application/json; charset=utf-8",
				async: false,
				dataType: "jsonp",
				success: function( response ) {

					if ( response.thumbnail_url ) {
						$container.prepend(
							'<div class="wp-embed-poster-wrap">' +
								'<img class="wp-embed-poster" src="' + response.thumbnail_url + '" />' +
								'<i class="far fa-play-circle"></i>' +
							'</div>'
						);
					}

					return;
				},
				error: function( e ) {
					//nothing found
				}
			});
		};


		/**
		 * Parse through default video blocks, adding featherlight and poster
		 */
		$( '.wp-block-video' ).each( function() {
			var $this = $(this);
			var $videoContent = $this.find('video');
			var poster = getPoster( $videoContent );

			// do nothing for videos in the body.
			if ( $this.parent().hasClass( 'entry-content' ) ) {
				return;
			}

			// Bind modal click event on video container
			$this.featherlight( $videoContent, {
				afterOpen: function() {
					$(this).find( 'video' ).attr( 'autoplay', 'true' );
				}
			} );

			// Append poster image
			if ( poster && ! $this.hasClass( 'posterized' ) ) {
				$this.prepend(
					'<div class="wp-embed-poster-wrap">' +
						'<img class="wp-embed-poster" src="' + poster + '" />' +
						'<i class="far fa-play-circle"></i>' +
					'</div>'
				);
				$this.addClass( 'posterized' );
			}
		});


		/**
		 * Parse YouTube video blocks
		 */
		$( '.wp-block-embed-youtube' ).each( function() {
			var $this = $(this);
			var $videoContent = $this.find( 'iframe' );
			var src = $videoContent.attr( 'src' );
			var poster = getPosterYouTube( src );

			// Bind modal click event on video container
			if ( typeof $this.featherlight === 'function' ) {
				$this.featherlight( $videoContent.clone().attr( 'src', src + '&autoplay=1' ) );
			}

			// Append poster image
			if ( poster && ! $this.hasClass( 'posterized' ) ) {
				$this.prepend(
					'<div class="wp-embed-poster-wrap">' +
						'<img class="wp-embed-poster" src="' + poster + '" />' +
						'<i class="far fa-play-circle"></i>' +
					'</div>'
				);
				$this.addClass( "posterized" );
			}
		});


		/**
		 * Parse Vimeo video blocks
		 */
		$( '.wp-block-embed-vimeo' ).each( function() {
			var $this = $(this);
			var $videoContent = $this.find( 'iframe' );
			var src = $videoContent.attr( 'src' );
			getPosterVimeo( $this, src );

			// bind modal click event on video container
			if ( typeof $this.featherlight === 'function' ) {
				$this.featherlight( $videoContent.clone().attr( 'src', src + '&autoplay=1' ) );
			}
		});


		/**
		 * Add play icon to audio blocks and related
		 */
		$( '.results' )
			.find( '.type-oep_cpts_video, .type-oep_cpts_podcast' )
			.find( '.post_thumbnail-wrap' )
			.append( '<i class="far fa-play-circle"></i>' );
	});
}( jQuery ) );

/**
 * Mobile navigation actions
 *
 * @since   1.0.0
 * @package oep
 */

(function($) {
	$(document).ready(function() {

		// things
		var $window         = $( window );
		var $header          = $( 'header[role=banner]' );
		var $nav             = $( 'nav', $header );
		var $navToggle       = $( '#menu-toggle', $header );
		var $adminBar        = $( '#wpadminbar' );
		var $navOverlay      = $( '.nav-overlay', $header );
		var $parentMenuItems = $( 'header .menu-item-has-children' );
		var $subMenuToggles;

		// heights
		var windowHeight;
		var headerHeight;
		var navHeight;
		var adminBarHeight;

		// init stuff
		maybeScrollY();
		addSubmenuToggles();

		// bind events
		$window.resize( maybeScrollY );
		$navToggle.click( toggleMenu );
		$subMenuToggles.click( toggleSubmenu );
		$navOverlay.click( toggleMenu );


		/**
		 * Toggler: toggle class and position menu
		 *
		 * @since 1.0.0
		 */
		function toggleMenu() {
			$nav.toggleClass( 'toggled' );
			$navToggle.toggleClass( 'toggled' );
			$navOverlay.fadeToggle();
		}


		/**
		 * Get the header, nav, and admin bar heights
		 *
		 * @since 1.0.0
		 */
		function getHeights() {
			windowHeight   = $window.height();
			headerHeight   = $header.outerHeight();
			navHeight      = $nav.outerHeight();
			adminBarHeight = $adminBar.outerHeight();
		}


		/**
		 * Maybe scroll overflow-y
		 *
		 * If the height of the menu goes beyond what's visible on the screen,
		 * allow some scrolling
		 *
		 * @since 1.0.0
		 */
		function maybeScrollY() {

			// reset scroll-y so height is calculated correctly
			$nav.removeClass( 'scroll-y' );

			// get our heights
			getHeights();

			// calculate usable height
			var usableHeight = windowHeight - headerHeight - adminBarHeight;

			// compare
			if ( usableHeight < navHeight ) {
				$nav.addClass( 'scroll-y' );
			}
		}


		/**
		 * Add mobile submenu dropdown toggles, then bind them to toggles var
		 *
		 * @since 1.0.0
		 */
		function addSubmenuToggles() {

			$parentMenuItems.append(
				'<button class="toggle-sub"><span class="screen-reader-text">Menu</span><i class="fas fa-caret-down"></i></button>'
			);

			$subMenuToggles = $( '.toggle-sub', $parentMenuItems );
		}


		/**
		 * Toggle mobile submenu
		 *
		 * @since 1.0.0
		 */
		function toggleSubmenu() {

			var $li = $( this ).parent();
			$li.toggleClass( 'sub-toggled' );

			// double-check the scroll-y
			$( '> .sub-menu', $li ).slideToggle( 300, maybeScrollY );
		}

	}); // document.ready
})(jQuery);

/**
 * Collapse main menu into compact version on scroll on desktop
 *
 * @since 1.0.0
 * @package oep
 */

(function ($) {

	var $header, $sitemenu;

	$( document ).ready( init );

	/**
	 * Spin up
	 */
	function init() {
		$header = $( 'header[role=banner]' );
		$sitemenu = $( '.site-menu', $header );

		if ( $header.length < 1 ) {
			return;
		}

		// check to see if menu has scrolled out of view
		$header.scrollspy({
			min: $sitemenu.offset().top,
			max: $( 'footer[role=contentinfo]' ).offset().top,
			onEnter: toggleScrollClass,
			onLeave: toggleScrollClass,
		});

		// do an on-load scroller check
		$header.scrollspy( 'refresh' );
	}

	/**
	 * Toggle scroll class
	 */
	function toggleScrollClass() {
		$header.toggleClass( 'scrolled' );
	}

})(jQuery);

/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function() {
	var container, button, menu, nav, links, subMenus, i, len;

	container = document.querySelector( 'header[role="banner"]' );
	if ( ! container ) {
		return;
	}

	button = container.getElementsByTagName( 'button' )[0];
	if ( 'undefined' === typeof button ) {
		return;
	}

	menu = container.getElementsByTagName( 'ul' )[0];
	nav = container.getElementsByTagName( 'nav' )[0];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	menu.setAttribute( 'aria-expanded', 'false' );
	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
		menu.className += ' nav-menu';
	}

	button.onclick = function() {
		if ( -1 !== nav.className.indexOf( 'toggled' ) ) {
			// nav.className = nav.className.replace( ' toggled', '' );
			button.setAttribute( 'aria-expanded', 'false' );
			menu.setAttribute( 'aria-expanded', 'false' );
		} else {
			// nav.className += ' toggled';
			button.setAttribute( 'aria-expanded', 'true' );
			menu.setAttribute( 'aria-expanded', 'true' );
		}
	};

	// Get all the link elements within the menu.
	links    = menu.getElementsByTagName( 'a' );
	subMenus = menu.getElementsByTagName( 'ul' );

	// Set menu items with submenus to aria-haspopup="true".
	for ( i = 0, len = subMenus.length; i < len; i++ ) {
		subMenus[i].parentNode.setAttribute( 'aria-haspopup', 'true' );
	}

	// Each time a menu link is focused or blurred, toggle focus.
	for ( i = 0, len = links.length; i < len; i++ ) {
		links[i].addEventListener( 'focus', toggleFocus, true );
		links[i].addEventListener( 'blur', toggleFocus, true );
	}

	/**
	 * Sets or removes .focus class on an element.
	 */
	function toggleFocus() {
		var self = this;

		// Move up through the ancestors of the current link until we hit .nav-menu.
		while ( -1 === self.className.indexOf( 'nav-menu' ) ) {

			// On li elements toggle the class .focus.
			if ( 'li' === self.tagName.toLowerCase() ) {
				if ( -1 !== self.className.indexOf( 'focus' ) ) {
					self.className = self.className.replace( ' focus', '' );
				} else {
					self.className += ' focus';
				}
			}

			self = self.parentElement;
		}
	}

	/**
	 * Toggles `focus` class to allow submenu access on tablets.
	 */
	( function( container ) {
		var touchStartFn, i,
			parentLink = container.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

		if ( 'ontouchstart' in window ) {
			touchStartFn = function( e ) {
				var menuItem = this.parentNode, i;

				if ( ! menuItem.classList.contains( 'focus' ) ) {
					e.preventDefault();
					for ( i = 0; i < menuItem.parentNode.children.length; ++i ) {
						if ( menuItem === menuItem.parentNode.children[i] ) {
							continue;
						}
						menuItem.parentNode.children[i].classList.remove( 'focus' );
					}
					menuItem.classList.add( 'focus' );
				} else {
					menuItem.classList.remove( 'focus' );
				}
			};

			for ( i = 0; i < parentLink.length; ++i ) {
				parentLink[i].addEventListener( 'touchstart', touchStartFn, false );
			}
		}
	}( container ) );
} )();

// from https://github.com/constancecchen/object-fit-polyfill
!function(){"use strict";if("objectFit"in document.documentElement.style!=!1)return void(window.objectFitPolyfill=function(){return!1});var t=function(t){var e=window.getComputedStyle(t,null),i=e.getPropertyValue("position"),o=e.getPropertyValue("overflow"),n=e.getPropertyValue("display");i&&"static"!==i||(t.style.position="relative"),"hidden"!==o&&(t.style.overflow="hidden"),n&&"inline"!==n||(t.style.display="block"),0===t.clientHeight&&(t.style.height="100%"),-1===t.className.indexOf("object-fit-polyfill")&&(t.className=t.className+" object-fit-polyfill")},e=function(t){var e=window.getComputedStyle(t,null),i={"max-width":"none","max-height":"none","min-width":"0px","min-height":"0px",top:"auto",right:"auto",bottom:"auto",left:"auto","margin-top":"0px","margin-right":"0px","margin-bottom":"0px","margin-left":"0px"};for(var o in i){e.getPropertyValue(o)!==i[o]&&(t.style[o]=i[o])}},i=function(t,e,i){var o,n,l,a,d;if(i=i.split(" "),i.length<2&&(i[1]=i[0]),"x"===t)o=i[0],n=i[1],l="left",a="right",d=e.clientWidth;else{if("y"!==t)return;o=i[1],n=i[0],l="top",a="bottom",d=e.clientHeight}return o===l||n===l?void(e.style[l]="0"):o===a||n===a?void(e.style[a]="0"):"center"===o||"50%"===o?(e.style[l]="50%",void(e.style["margin-"+l]=d/-2+"px")):o.indexOf("%")>=0?(o=parseInt(o),void(o<50?(e.style[l]=o+"%",e.style["margin-"+l]=d*(o/-100)+"px"):(o=100-o,e.style[a]=o+"%",e.style["margin-"+a]=d*(o/-100)+"px"))):void(e.style[l]=o)},o=function(o){var n=o.dataset?o.dataset.objectFit:o.getAttribute("data-object-fit"),l=o.dataset?o.dataset.objectPosition:o.getAttribute("data-object-position");n=n||"cover",l=l||"50% 50%";var a=o.parentNode;t(a),e(o),o.style.position="absolute",o.style.height="100%",o.style.width="auto","scale-down"===n&&(o.style.height="auto",o.clientWidth<a.clientWidth&&o.clientHeight<a.clientHeight?(i("x",o,l),i("y",o,l)):(n="contain",o.style.height="100%")),"none"===n?(o.style.width="auto",o.style.height="auto",i("x",o,l),i("y",o,l)):"cover"===n&&o.clientWidth>a.clientWidth||"contain"===n&&o.clientWidth<a.clientWidth?(o.style.top="0",o.style.marginTop="0",i("x",o,l)):"scale-down"!==n&&(o.style.width="100%",o.style.height="auto",o.style.left="0",o.style.marginLeft="0",i("y",o,l))},n=function(){for(var t=document.querySelectorAll("[data-object-fit]"),e=0;e<t.length;e++){var i=t[e].nodeName.toLowerCase();"img"===i?t[e].complete?o(t[e]):t[e].addEventListener("load",function(){o(this)}):"video"===i&&(t[e].readyState>0?o(t[e]):t[e].addEventListener("loadedmetadata",function(){o(this)}))}return!0};document.addEventListener("DOMContentLoaded",function(){n()}),window.addEventListener("resize",function(){n()}),window.objectFitPolyfill=n}();
/**
 * Pill colors
 *
 * @since   1.0.0
 * @package oep
 */

(function ($) {
  $('li.pills a').on('click', function () {
    $(this).parent().toggleClass('expanded');
    $('ul.terms li').first().toggleClass('hidden');
  })
})(jQuery);

/**
 * Initialize QuickLink
 */
(function () {
  // quicklink();
})();
/**
 * Read More
 *
 * @since   1.0.0
 * @package oep
 */


(function($) {
	$(document).ready(function() {

		var $showMore = $('span[id*="more-"]');
		$showMore.html("More...");
		$showMore.parent().addClass('read-more-link');
		var $showMoreNext = $showMore.parent().nextAll();
		$showMoreNext.hide();

		$showMore.on('click', function() {
			$showMoreNext.toggle("slow");
		});

	}); // document.ready
})(jQuery);

/**
 * Toggle search visibility on mobile
 */

( function($) {
    $("#search-toggle").on('click', function(){
        $search = $('#search');
        if($search.hasClass("-active")) {
            $search.removeClass("-active");
        } else {
            $search.addClass("-active").find('input').trigger('focus');
        }
    });
})(jQuery);
/**
 * Sidebar menu.
 *
 * Handles toggling the sidebar menu navigation support for dropdown menus.
 */
( function($) {
    var $menuSidebar = $('#menu-sidebar'),
        activeClass = '-active';

    // Hide submenus by default
    $menuSidebar.find('.sub-menu').hide();

    // On click of hamburger, add Class that will translate menu into view
    $('#hamburger-menu').on('click', function(){
        $menuSidebar.addClass(activeClass);
    });

    // On click of close, remove Class that will translate menu out of view
    $('#menu-sidebar_close').on('click', function(){
      $menuSidebar.removeClass(activeClass);  
    });

    // On click of menu item that has a submenu, add or remove Class to show submenu
    $menuSidebar.find('.menu-item-has-children').children('a').prepend('<i class="fas fa-angle-down"></i>').on('click', function(event){
        event.preventDefault();
        var $this = $(this);

        if($this.hasClass(activeClass)) {
            $this.removeClass(activeClass).next('.sub-menu').slideUp();
        } else {
            $this.addClass(activeClass).next('.sub-menu').slideDown();
            $this.parent().siblings('.menu-item-has-children').find('.-active').removeClass(activeClass).next('.sub-menu').slideUp();
        }
    }).first().trigger('click'); // Ensure first menu item is open on page load
}( jQuery ) );

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
( function() {
	var isWebkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
	    isOpera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
	    isIe     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

	if ( ( isWebkit || isOpera || isIe ) && document.getElementById && window.addEventListener ) {
		window.addEventListener( 'hashchange', function() {
			var id = location.hash.substring( 1 ),
				element;

			if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
				return;
			}

			element = document.getElementById( id );

			if ( element ) {
				if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false );
	}
})();

( function($) {
if (!('flickity' in $.fn)) {
  return;
}

var $carouselElement = $('.slider');
if (!$carouselElement) {//no carousel on this page
  //do nothing
} else {
  //disable default event for nav item
  $("ul.nav-slider li.nav_item a").click(function(e){
    e.preventDefault();
  });
  var autoPlaySpeed = 5000;
  var transInt;

  var options = {
    autoPlay: autoPlaySpeed,
    setGallerySize: false,
    contain: true,
    wrapAround: false,
    prevNextButtons: false,
    pageDots: true,
    draggable: true,
    cellSelector: 'article',
    selectedAttraction: 0.01,
    friction: 0.15
  };

  if (matchMedia('screen and (min-width: 768px)').matches) {
    // options.prevNextButtons = true;
    // options.pageDots = false;
  }

  var $carousel = $carouselElement.flickity(options);

}
})(jQuery);

/**
 * Dropdown trigger for "more" terms menu on landing pages
 * This function switches out the name of the select element with the taxonomy the term belongs to to facilitate having a dropdown with terms from multiple taxonomies.
 *
 * @since   1.0.0
 * @package oep
 */

(function($) {
    $(document).ready(function() {
        var $form = $("#term-dropdown-select");
        
        $form.find('select').on('change', function(event){
            $(this).attr("name", $(this).find(':selected').data('taxonomy'));
            $form.submit();
        })
    }); // document.ready
})(jQuery);

(function ($) {

    function getIconFromForecast( forecast, isDayTime ) {
        var fa_icon = '<span><i class="' + "fas" +' fa-' + "sun" + '"></i></span>'; //default
        var timeOfDay = isDayTime == true ? "sun" : "moon";
        var iconOptions = {};

        iconOptions[timeOfDay] = ["hot", "sunny", "sun", "fair", "clear", "mostly clear"];
        iconOptions[timeOfDay + "-cloud"] = ["few", "clouds"];
        iconOptions["clouds"] = ["partly cloudy", "mostly cloudy", "overcast"];
        iconOptions["raindrops"] = ["rain"];
        iconOptions["thunderstorm-" + timeOfDay] = ["thunder"];

        $.each(iconOptions, function (key, val) {
            if(val.indexOf(forecast.toLowerCase()) > 0){
                // console.log(forecast.toLowerCase());
                // console.log(val);
                fa_icon = '<i class="' + "fas" +' fa-' + key + '"></i>';
            }
        });
        return fa_icon;
    }

    function jsonify (){

        var $widget          = $( 'div.weather' );
        var $url             = "https://api.weather.gov/gridpoints/MLB/25,68/forecast";


        $.getJSON( $url, function( data ) {
            var icon;
            var shortForecast;
            var temp ;
            var items = [];

            $.each( data.properties.periods[0], function( key, val ) {
                items[key] = val;
            });

            shortForecast = items["shortForecast"];
            icon = getIconFromForecast(shortForecast, items["isDaytime"]);
            temp = items["temperature"] + '&deg;' + items["temperatureUnit"];
            $( '.degrees', $widget ).html( temp );
            // $( '.conditions', $widget ).html( shortForecast );
            $( '.conditions', $widget ).prepend( icon );
        });
    }
    jsonify();
})(jQuery);
