/**
 * Add Font Awesome icons to stuff
 *
 * @since   1.0.0
 * @package bigblue
 */

(function($) {
	$(document).ready(function() {

		$.each({
			'.entry-content > ul li, ul.bulleted li' : ['circle', 				'fas'],
			// '.wp-block-column ul li'				 : ['circle', 				'fas'],
			'.wp-block-oep-headline-text ul li'		 : ['circle', 				'fas'],
			'.wp-block-oep-event-text ul li'		 : ['circle', 				'fas'],
			'.wp-block-oep-text-text ul li'			 : ['circle', 				'fas'],
			'.editor-rich-text ul li' 				 : ['circle', 				'fas'],
			'.validation_message'                    : ['exclamation-triangle', 'fas'],
			'.ginput_container_date'                 : ['calendar-alt', 		'fas'],
			'.event_detail.--address'				 : ['map-marker-alt', 		'fal'],
			'.event_detail.--date'					 : ['clock', 				'fal'],
			'#menu-global-sidebar .phone-link a'	 : ['phone', 				'fas'],
		}, prependIcon );

		$.each({
			'.entry-content a[target=_blank]'					 	: ['external-link-alt', 'fas'],
			'.tiles.wide.in-content article'						: ['arrow-right', 'fal'],
			'.featured .row article'								: ['arrow-right', 'fal'],
			'.--featured ul.posts li.post'							: ['arrow-right', 'fal'],
			'.--upcoming-events .post_thumbnail'					: ['arrow-right', 'fal'],
			'.--upcoming-events .more-link'							: ['arrow-right', 'fal'],
		    '.oep_cpts_event .post_thumbnail-wrap'      			: ['arrow-right', 'fal'],
			'.recent-posts.has-stories .more-link'								: ['arrow-right', 'fal'],
			'.wp-block-oep-column-sidebar .more-link a'				: ['arrow-right', 'fal'],
			'.wp-block-oep-column-sidebar .recent-posts a.more-link'				: ['arrow-right', 'fal'],
			'.post-type-archive-oep_cpts_event .event_details'		: ['arrow-right', 'fal'],
			'.wp-block-oep-slider .more-link'						: ['arrow-right', 'fal'],
			'header.banner.slider header.entry-header .more-link'	: ['arrow-right', 'fal'],
			'.recent-posts .featured'								: ['arrow-right', 'fal'],
			'.has-stories.has-3-posts article'						: ['arrow-right', 'fal'],
			'.has-stories.has-4-posts article:first-child'			: ['arrow-right', 'fal'],
			'.has-stories.has-6-posts article'						: ['arrow-right', 'fal'],
			'.wp-block-oep-general-text-header .text-only .wp-block-button__link' : ['arrow-right', 'far'],
		}, appendIcon );

		/**
		 * Prepend an icon to a jQuery object
		 *
		 * @param {string} thing selector
		 * @param {string} icon  name of Font Awesome icon (like file-text)
		 * @param {string} collection  of Font Awesome icon (like fas, fab, fal etc)
		 */
		function prependIcon( thing, icon ) {
			$( thing ).addClass('xxx').prepend( '<span class="icon-wrap"><i class="' + icon[1] +' fa-' + icon[0] + '"></i></span>' );
		}

		/**
		 * Append an icon to a jQuery object
		 *
		 * @param {string} thing selector
		 * @param {string} icon  name of Font Awesome icon (like file-text)
		 * @param {string} collection  collection of Font Awesome icon (like fas, fab, fal etc)
		 */
		function appendIcon( thing, icon ) {
			$( thing ).append( '<span class="icon-wrap"><i class="' + icon[1] +' fa-' + icon[0] + '"></i></span>' );
		}

		//resize bulleted lists
		$('ul.bulleted').addClass("fa-ul");
		$('ul.bulleted li span').addClass("fa-li");
		$('ul.bulleted li span i').addClass("fa-xs");

		//recolor heading svgs
		$('div.svg.heading svg').attr('fill', $('div.accent').data('color'));
		$('div.svg.heading svg').attr('style', 'fill: "' + $('div.accent').data('color') + '"');

		$('div.svg.heading-accent svg').each(function(index){
			$( this ).attr('fill', $(this).parent().parent().data('color'))
		});
		// $('div.svg.heading-accent svg').attr('style', 'fill: "' + $('div.accent').data('color') + '"');

	}); // document.ready
})(jQuery);
