/**
 * Form interactions
 *
 * @since   1.0.0
 * @package oep
 */
(function($) {

	var $document = $( document );
	$document.ready( init );

	/**
	 * Start setting/binding document-ready stuff
	 *
	 * @since 1.0.0
	 */
	function init() {

		var $body       = $( document.body );
		var $prefixes   = $( '.field-prefix' );
		var $checkboxes = $( '[type="checkbox"], [type="radio"]' );

		$prefixes.each( doPrefixSpace );
		$checkboxes.each( checkCheckboxes );
		$checkboxes.on( 'change', checkCheckboxes );
		$body.on( 'update_checkout', checkCheckboxes );
	}


	/**
	 * Apply field prefix spacing
	 *
	 * @since 1.1.1
	 */
	function doPrefixSpace() {

		var $this = $(this);
		var padding = parseInt( $this.css( 'paddingLeft' ) ) + $this.width() + 1 + 'px';

		$this.next( 'input' ).css( 'paddingLeft', padding );
	}


	/**
	 * See if checkboxes and radios are checked, flagging a parent <label>
	 *
	 * @since 1.0.0
	 */
	function checkCheckboxes() {

		$this = $(this);

		checked = $this.prop( 'checked' );
		$label  = $this.parent( 'label' );

		if ( checked ) {
			$label.addClass( 'input-checked' );
		} else {
			$label.removeClass( 'input-checked' );
		}
	}

})(jQuery);
