/**
 * Add Font Awesome icons to stuff
 *
 * @since   1.0.0
 * @package bigblue
 */

(function($) {
    $(document).ready(function() {

        $('.wp-block-audio').each(function(){
            var $audio = $(this).find('audio');
            var $link = $('<a class="audio-link" href="' + $audio.attr("src") + '">Download Podcast</a>');
            $audio.before($link);
        });

    }); // document.ready
})(jQuery);
