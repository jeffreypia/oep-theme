/**
 * Pill colors
 *
 * @since   1.0.0
 * @package oep
 */

(function ($) {
  $('li.pills a').on('click', function () {
    $(this).parent().toggleClass('expanded');
    $('ul.terms li').first().toggleClass('hidden');
  })
})(jQuery);
