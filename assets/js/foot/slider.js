( function($) {
if (!('flickity' in $.fn)) {
  return;
}

var $carouselElement = $('.slider');
if (!$carouselElement) {//no carousel on this page
  //do nothing
} else {
  //disable default event for nav item
  $("ul.nav-slider li.nav_item a").click(function(e){
    e.preventDefault();
  });
  var autoPlaySpeed = 5000;
  var transInt;

  var options = {
    autoPlay: autoPlaySpeed,
    setGallerySize: false,
    contain: true,
    wrapAround: false,
    prevNextButtons: false,
    pageDots: true,
    draggable: true,
    cellSelector: 'article',
    selectedAttraction: 0.01,
    friction: 0.15
  };

  if (matchMedia('screen and (min-width: 768px)').matches) {
    // options.prevNextButtons = true;
    // options.pageDots = false;
  }

  var $carousel = $carouselElement.flickity(options);

}
})(jQuery);
