/**
 * Video Gutenberg blocks
 *
 * Launches video content in a lightbox
 */
( function($) {
	$(document).on('ready', function () {

		/**
		 * Get the poster attribute of a native <video>
		 *
		 * If it's a video block and poster image has been set, the URL should
		 * be in a 'poster' attribute.
		 *
		 * @param  {jQuery} $video
		 * @return {string} poster prop image URL
		 */
		var getPoster = function( $video ) {

			if ( $video[0].hasAttribute( 'poster' ) ) {
				return $video.attr( 'poster' );
			} else {
				return false;
			}
		};


		/**
		 * Grab the ID from YouTube SRC and grab the thumbnail URL for it.
		 *
		 * @see    https://stackoverflow.com/questions/5817505/is-there-any-method-to-get-the-url-without-query-string
		 * @param {string} url
		 */
		var getPosterYouTube = function( url ) {

			if ( ! url ) {
				return;
			}

			var id = url.split( '?' )[0].split( '/' ).pop();
			return 'https://img.youtube.com/vi/' + id + '/maxresdefault.jpg';
		};


		/**
		 * Get Vimeo video thumbnail via API and output it in wrapper
		 *
		 * This is an embed, so extract ID from src attr and try try to
		 * construct an image URL
		 *
		 * @see   https://stackoverflow.com/questions/2916544/parsing-a-vimeo-id-using-javascript
		 * @param {jQuery} $container
		 * @param {string} url
		 */
		var getPosterVimeo = function( $container, url ) {

			var id = url.split('?')[0].split('/').pop();

			$.ajax({
				type: "GET",
				url: 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/' + id,
				contentType: "application/json; charset=utf-8",
				async: false,
				dataType: "jsonp",
				success: function( response ) {

					if ( response.thumbnail_url ) {
						$container.prepend(
							'<div class="wp-embed-poster-wrap">' +
								'<img class="wp-embed-poster" src="' + response.thumbnail_url + '" />' +
								'<i class="far fa-play-circle"></i>' +
							'</div>'
						);
					}

					return;
				},
				error: function( e ) {
					//nothing found
				}
			});
		};


		/**
		 * Parse through default video blocks, adding featherlight and poster
		 */
		$( '.wp-block-video' ).each( function() {
			var $this = $(this);
			var $videoContent = $this.find('video');
			var poster = getPoster( $videoContent );

			// do nothing for videos in the body.
			if ( $this.parent().hasClass( 'entry-content' ) ) {
				return;
			}

			// Bind modal click event on video container
			$this.featherlight( $videoContent, {
				afterOpen: function() {
					$(this).find( 'video' ).attr( 'autoplay', 'true' );
				}
			} );

			// Append poster image
			if ( poster && ! $this.hasClass( 'posterized' ) ) {
				$this.prepend(
					'<div class="wp-embed-poster-wrap">' +
						'<img class="wp-embed-poster" src="' + poster + '" />' +
						'<i class="far fa-play-circle"></i>' +
					'</div>'
				);
				$this.addClass( 'posterized' );
			}
		});


		/**
		 * Parse YouTube video blocks
		 */
		$( '.wp-block-embed-youtube' ).each( function() {
			var $this = $(this);
			var $videoContent = $this.find( 'iframe' );
			var src = $videoContent.attr( 'src' );
			var poster = getPosterYouTube( src );

			// Bind modal click event on video container
			if ( typeof $this.featherlight === 'function' ) {
				$this.featherlight( $videoContent.clone().attr( 'src', src + '&autoplay=1' ) );
			}

			// Append poster image
			if ( poster && ! $this.hasClass( 'posterized' ) ) {
				$this.prepend(
					'<div class="wp-embed-poster-wrap">' +
						'<img class="wp-embed-poster" src="' + poster + '" />' +
						'<i class="far fa-play-circle"></i>' +
					'</div>'
				);
				$this.addClass( "posterized" );
			}
		});


		/**
		 * Parse Vimeo video blocks
		 */
		$( '.wp-block-embed-vimeo' ).each( function() {
			var $this = $(this);
			var $videoContent = $this.find( 'iframe' );
			var src = $videoContent.attr( 'src' );
			getPosterVimeo( $this, src );

			// bind modal click event on video container
			if ( typeof $this.featherlight === 'function' ) {
				$this.featherlight( $videoContent.clone().attr( 'src', src + '&autoplay=1' ) );
			}
		});


		/**
		 * Add play icon to audio blocks and related
		 */
		$( '.results' )
			.find( '.type-oep_cpts_video, .type-oep_cpts_podcast' )
			.find( '.post_thumbnail-wrap' )
			.append( '<i class="far fa-play-circle"></i>' );
	});
}( jQuery ) );
