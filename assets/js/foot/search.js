/**
 * Toggle search visibility on mobile
 */

( function($) {
    $("#search-toggle").on('click', function(){
        $search = $('#search');
        if($search.hasClass("-active")) {
            $search.removeClass("-active");
        } else {
            $search.addClass("-active").find('input').trigger('focus');
        }
    });
})(jQuery);