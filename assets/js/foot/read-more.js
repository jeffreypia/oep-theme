/**
 * Read More
 *
 * @since   1.0.0
 * @package oep
 */


(function($) {
	$(document).ready(function() {

		var $showMore = $('span[id*="more-"]');
		$showMore.html("More...");
		$showMore.parent().addClass('read-more-link');
		var $showMoreNext = $showMore.parent().nextAll();
		$showMoreNext.hide();

		$showMore.on('click', function() {
			$showMoreNext.toggle("slow");
		});

	}); // document.ready
})(jQuery);
