/**
 * Collapse main menu into compact version on scroll on desktop
 *
 * @since 1.0.0
 * @package oep
 */

(function ($) {

	var $header, $sitemenu;

	$( document ).ready( init );

	/**
	 * Spin up
	 */
	function init() {
		$header = $( 'header[role=banner]' );
		$sitemenu = $( '.site-menu', $header );

		if ( $header.length < 1 ) {
			return;
		}

		// check to see if menu has scrolled out of view
		$header.scrollspy({
			min: $sitemenu.offset().top,
			max: $( 'footer[role=contentinfo]' ).offset().top,
			onEnter: toggleScrollClass,
			onLeave: toggleScrollClass,
		});

		// do an on-load scroller check
		$header.scrollspy( 'refresh' );
	}

	/**
	 * Toggle scroll class
	 */
	function toggleScrollClass() {
		$header.toggleClass( 'scrolled' );
	}

})(jQuery);
