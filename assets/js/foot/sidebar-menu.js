/**
 * Sidebar menu.
 *
 * Handles toggling the sidebar menu navigation support for dropdown menus.
 */
( function($) {
    var $menuSidebar = $('#menu-sidebar'),
        activeClass = '-active';

    // Hide submenus by default
    $menuSidebar.find('.sub-menu').hide();

    // On click of hamburger, add Class that will translate menu into view
    $('#hamburger-menu').on('click', function(){
        $menuSidebar.addClass(activeClass);
    });

    // On click of close, remove Class that will translate menu out of view
    $('#menu-sidebar_close').on('click', function(){
      $menuSidebar.removeClass(activeClass);  
    });

    // On click of menu item that has a submenu, add or remove Class to show submenu
    $menuSidebar.find('.menu-item-has-children').children('a').prepend('<i class="fas fa-angle-down"></i>').on('click', function(event){
        event.preventDefault();
        var $this = $(this);

        if($this.hasClass(activeClass)) {
            $this.removeClass(activeClass).next('.sub-menu').slideUp();
        } else {
            $this.addClass(activeClass).next('.sub-menu').slideDown();
            $this.parent().siblings('.menu-item-has-children').find('.-active').removeClass(activeClass).next('.sub-menu').slideUp();
        }
    }).first().trigger('click'); // Ensure first menu item is open on page load
}( jQuery ) );
