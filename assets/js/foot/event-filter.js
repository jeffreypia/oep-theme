/**
 * Filter for Events listing page
 *
 * @since   1.0.0
 * @package oep
 */

(function($) {
    $(document).ready(function() {
        var $form = $("#eventFilter");
        
        $form.find('select').on('change', function(event){
            $form.submit();
        })
    }); // document.ready
})(jQuery);
