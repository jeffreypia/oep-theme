/**
 * Dropdown trigger for "more" terms menu on landing pages
 * This function switches out the name of the select element with the taxonomy the term belongs to to facilitate having a dropdown with terms from multiple taxonomies.
 *
 * @since   1.0.0
 * @package oep
 */

(function($) {
    $(document).ready(function() {
        var $form = $("#term-dropdown-select");
        
        $form.find('select').on('change', function(event){
            $(this).attr("name", $(this).find(':selected').data('taxonomy'));
            $form.submit();
        })
    }); // document.ready
})(jQuery);
