/**
 * Ajax Pagination
 * Because Johnny asked nicely
 *
 * @since   1.0.0
 * @package oep
 */

(function ($) {

  var $types = ($('main div.results > div > select').length > 0) ? $('main div.results > div > select') : $('input.types');
  console.log($types);
  var $filters = $('.filters select');

  function find_page_number( element ) {
    if(element.find('span').length > 0) {
      element.find('span').remove();
      return parseInt(element.html());
    } else {
      return 1;
    }
  }

  function ajaxify ( e ){
    e.preventDefault();

    var $header          = $( 'header[role=banner]' );
    var $headerHeight   = $header.outerHeight();
    var $target = $('main .results');
    var $scrollToPosition = 0;

    page = find_page_number( $(this).clone() );
    spinner_url = ajaxpagination.spinner_url;


    $.ajax({
      url: ajaxpagination.ajaxurl,
      type: 'post',
      data: {
        action: 'ajax_pagination',
        query_vars: ajaxpagination.query_vars,
        page: page,
        base_url: ajaxpagination.base_url,
        post_type: $types.val(),
        oep_taxonomies_industries: $('[name=oep_taxonomies_industries]').val(),
        oep_taxonomies_size: $('[name=oep_taxonomies_size]').val(),
        oep_taxonomies_regions: $('[name=oep_taxonomies_regions]').val(),
      },
      beforeSend: function() {
        $scrollToPosition = $target.offset().top;
        $('html, body').animate({
          scrollTop: $scrollToPosition - $headerHeight,
        }, 500);
        $target.toggleClass('hidden');
        $target.find('nav').remove();
        $target.append( '<div class="pagination-overlay" id="loader"><img src="'+ spinner_url +'" /></div>' );
      },
      success: function( html ) {
        $target.find( 'article' ).remove();
        $target.find('nav').remove();
        $('main #loader').remove();
        $target.append( html );
        $target.toggleClass('hidden');

        //do it all over again
        $('.posts-navigation.numbered-pagination ul li a').on( 'click', ajaxify );
      }
    });
  }
  // event handlers
  $('.posts-navigation.numbered-pagination ul li a').on( 'click', ajaxify );
  $types.change( ajaxify );
  $filters.change( ajaxify );
})(jQuery);