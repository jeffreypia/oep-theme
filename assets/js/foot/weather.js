(function ($) {

    function getIconFromForecast( forecast, isDayTime ) {
        var fa_icon = '<span><i class="' + "fas" +' fa-' + "sun" + '"></i></span>'; //default
        var timeOfDay = isDayTime == true ? "sun" : "moon";
        var iconOptions = {};

        iconOptions[timeOfDay] = ["hot", "sunny", "sun", "fair", "clear", "mostly clear"];
        iconOptions[timeOfDay + "-cloud"] = ["few", "clouds"];
        iconOptions["clouds"] = ["partly cloudy", "mostly cloudy", "overcast"];
        iconOptions["raindrops"] = ["rain"];
        iconOptions["thunderstorm-" + timeOfDay] = ["thunder"];

        $.each(iconOptions, function (key, val) {
            if(val.indexOf(forecast.toLowerCase()) > 0){
                // console.log(forecast.toLowerCase());
                // console.log(val);
                fa_icon = '<i class="' + "fas" +' fa-' + key + '"></i>';
            }
        });
        return fa_icon;
    }

    function jsonify (){

        var $widget          = $( 'div.weather' );
        var $url             = "https://api.weather.gov/gridpoints/MLB/25,68/forecast";


        $.getJSON( $url, function( data ) {
            var icon;
            var shortForecast;
            var temp ;
            var items = [];

            $.each( data.properties.periods[0], function( key, val ) {
                items[key] = val;
            });

            shortForecast = items["shortForecast"];
            icon = getIconFromForecast(shortForecast, items["isDaytime"]);
            temp = items["temperature"] + '&deg;' + items["temperatureUnit"];
            $( '.degrees', $widget ).html( temp );
            // $( '.conditions', $widget ).html( shortForecast );
            $( '.conditions', $widget ).prepend( icon );
        });
    }
    jsonify();
})(jQuery);
