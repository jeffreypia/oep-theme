/**
 * Add To Any Sticky icons
 *
 * @since   1.0.0
 * @package oep
 */

(function ($) {
  $(document).ready(function () {
    var $nav = $('.a2a_kit');

    if ( $nav.length < 1 ) {
      //no add to any icons here, bounce
      return;
    }

    //create a duplicate for sharing in the footer on mobile devices
    $('.entry-footer').prepend($nav.clone().addClass("content-end")).prepend($('span.mobile').clone().addClass("content-end"));

    //add a class to our original set of icons
    $nav.addClass("in-banner");

    $nav.find('span').each(function () {
      $(this).attr('data-color', $(this).css('backgroundColor'));
      $(this).css('backgroundColor', 'transparent');
    });

    $('#content > header.banner').scrollspy({
      min: $nav.offset().top,
      max: $('footer').offset().top,
      onEnter: function (element, position) {
        $nav.addClass('fixed');
        $nav.find('span').each(function () {
          $(this).css('backgroundColor', $(this).attr('data-color'));
        });
      },
      onLeave: function (element, position) {
        $nav.removeClass('fixed');
        $nav.find('span').each(function () {
          $nav.find('span').css('backgroundColor','transparent');
        });
      }
    });

	//make social share section full color at all times
	$('#content .social-share .a2a_kit').find('span').each(function () {
    $(this).css('backgroundColor', $(this).attr('data-color'));
	});
	
  });
})(jQuery);
