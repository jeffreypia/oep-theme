<?php
/**
 * The template for displaying search results pages.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * @since   1.0.0
 * @package oep
 */

get_header(); ?>

    <section id="primary" class="content-area">
        <main id="main" class="site-main wrap" role="main">

			<?php
			if ( have_posts() ) : global $count; ?>

            <header class="page-header">
				<?php
				do_action( "oep_search_form_full" );
				?>
            </header><!-- .page-header -->

            <div class="results">
                <h3><?php
                    $post_type = get_post_type_object( get_query_var( 'post_type') ) ? __("All ", "oep").'<span class="type">'.get_post_type_labels( get_post_type_object( get_query_var( 'post_type') ) )->menu_name.'</span>' : __("Results", "oep");
                    $keyword = '<span class="keyword">"' . get_search_query() . '"</span>';
                    $type = get_query_var( OEP_BLOG_TYPES_KEY ) ? __("All ", "oep").'<span class="type">'.get_term_by( 'slug', get_query_var( OEP_BLOG_TYPES_KEY ), OEP_BLOG_TYPES_KEY )->name.'s</span>' : $post_type;
                    $in = get_query_var( OEP_QOL_KEY) ? __("in ", "oep").'<span class="in">'.get_term_by( 'slug', get_query_var( OEP_QOL_KEY ), OEP_QOL_KEY )->name.'</span>' : '';
					echo sprintf(
					        esc_html__('Showing %s for %s %s', 'oep' ),
                            $type, $keyword, $in);
					?></h3>

				<?php
				/* Start the Loop */
				$count = 0;
				while ( have_posts() ) : the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content-grid', get_post_type() );

				endwhile;

				oep_numbered_pagination();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>
            </div>
        </main><!-- #main -->
    </section><!-- #primary -->

<?php get_footer();
