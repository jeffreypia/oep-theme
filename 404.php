<?php
/**
 * 404 Page Template
 *
 * @since   1.0.0
 * @package oep
 */

get_header();
?>

	<?php oep_section( 'banner' ); ?>

	<main role="main">

		<?php oep_section( 'error' ); ?>

	</main>

<?php get_footer();
