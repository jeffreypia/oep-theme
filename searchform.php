<?php
/**
 * Search form override
 *
 * @since   1.0.0
 * @package oep
 */

oep_active() ? OEP\Common\Search::do_form() : null;
